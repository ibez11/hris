'use strict';
var {mongoDB} = require("./");
var table = 'announcements'

var Schema = mongoDB.Schema;
var AnnouncementSchema = new Schema({
    _id: {type: Schema.Types.ObjectId},
    title: String,
    short_description: String,
    description: String,
    is_published: {type: Boolean, default: false},
    is_published_at: String,
    is_published_by: {type: Schema.Types.ObjectId},
    created_at: String,
    created_by: {type: Schema.Types.ObjectId},
    updated_at: {type: String, default: null},
    updated_by: {type: Schema.Types.ObjectId}
});

var Announcement = mongoDB.hris.model(table, AnnouncementSchema, table);
module.exports.Announcement = Announcement;