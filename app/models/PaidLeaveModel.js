'use strict';
var {mongoDB} = require("./");
var table = 'paidLeave'

var Schema = mongoDB.Schema;
var PaidLeaveSchema = new Schema({
    start_date: String,
    end_date: {type: String, default: ''},
    employee_id: {type: Schema.Types.ObjectId},
    category_leave_id: {type: Number, default: null},
    status_id: {type: Number, default: 31}, // 0 -> Pending 2 -> Accepted 4 -> Rejected
    notes: {type: String, default: ''},
    is_status_changed_at: {type: String, default: ''},
    is_status_changed_by: {type: Schema.Types.ObjectId},
    date_of_filing: {type: String, default: ''},
    created_at: {type: String, default: ''},
    created_by: {type: Schema.Types.ObjectId},
    updated_at: {type: String, default: ''},
    updated_by: {type: Schema.Types.ObjectId},
});

PaidLeaveSchema.virtual('status_info',{
    ref: 'categories',
    localField: 'status_id',
    foreignField: '_id',
    justOne: true
});

PaidLeaveSchema.virtual('employee_info',{
    ref: 'employees',
    localField: 'employee_id',
    foreignField: '_id',
    justOne: true
});

PaidLeaveSchema.set('toObject', { virtuals: true });
PaidLeaveSchema.set('toJSON', { virtuals: true });

var PaidLeave = mongoDB.hris.model(table, PaidLeaveSchema, table);
module.exports.PaidLeave = PaidLeave;