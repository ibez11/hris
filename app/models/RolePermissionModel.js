'use strict';
var {mongoDB} = require("./");

var Schema = mongoDB.Schema;
var RolePermissionSchema = new Schema({
    role_id: {type: Number},
    permission_id: {type: Number},
    created_at: {type: String, default: ''},
    updated_at: {type: String, default: ''}
});

RolePermissionSchema.virtual('category',{
    ref: 'categories',
    localField: 'permission_id',
    foreignField: '_id',
    default: {}
});

RolePermissionSchema.set('toObject', { virtuals: true });
RolePermissionSchema.set('toJSON', { virtuals: true });

var RolePermission = mongoDB.hris.model(`rolePermissions`, RolePermissionSchema, `rolePermissions`);
module.exports.RolePermission = RolePermission;