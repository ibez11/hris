'use strict';
var {mongoDB} = require("./");
var table = 'allowanceDeductionDistributions';

mongoDB.set('useCreateIndex', true);
var Schema = mongoDB.Schema;
var AllowanceDeductionDistributionSchema = new Schema({
    name: {type: String, default: null},
    is_allowance: {type: Boolean, default: true},
    amount: {type: Number, default: 0},
    month: {type: String, default: null},
    employee_id: {type: Schema.Types.ObjectId, default: null},
    salary_description_id: {type: Schema.Types.ObjectId, default: null},
    created_at: {type: String, default: null},
    created_by: {type: Schema.Types.ObjectId},
    updated_at: {type: String, default: null},
    updated_by: {type: Schema.Types.ObjectId}
});

var AllowanceDeductionDistribution = mongoDB.hris.model(table, AllowanceDeductionDistributionSchema, table);
module.exports.AllowanceDeductionDistribution = AllowanceDeductionDistribution;