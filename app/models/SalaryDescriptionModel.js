'use strict';
var {mongoDB} = require("./");
var table = 'salaryDescriptions';

mongoDB.set('useCreateIndex', true);
var Schema = mongoDB.Schema;
var SalaryDescriptionSchema = new Schema({
    _id: {type: Schema.Types.ObjectId},
    name: {type: String, default: null},
    code: {type: String, default: "+"},
    description: {type: String, default: null},
    created_at: {type: String, default: null},
    created_by: {type: Schema.Types.ObjectId},
    updated_at: {type: String, default: null},
    updated_by: {type: Schema.Types.ObjectId}
});

var SalaryDescription = mongoDB.hris.model(table, SalaryDescriptionSchema, table);
module.exports.SalaryDescription = SalaryDescription;