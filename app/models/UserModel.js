'use strict';
var {mongoDB} = require("./");

mongoDB.set('useCreateIndex', true);
var Schema = mongoDB.Schema;
var UserSchema = new Schema({
    full_name: String,
    username: {type: String, index: true, unique: true},
    password: String,
    role_type: {type: Number, default: 6},
    is_actived: {type: Boolean, default: false},
    created_at: {type: String, default: ''},
    created_by: {type: Schema.Types.ObjectId},
    updated_at: {type: String, default: ''},
    updated_by: {type: Schema.Types.ObjectId},
});

UserSchema.virtual('role_permission',{
    ref: 'rolePermissions',
    localField: 'role_type',
    foreignField: 'role_id',
    default: {}
});

UserSchema.virtual('role_type_code',{
    ref: 'roleTypes',
    localField: 'role_type',
    foreignField: 'code',
    justOne: true
});

UserSchema.virtual('employee_info',{
    ref: 'employees',
    localField: '_id',
    foreignField: '_id',
    justOne: true
});

UserSchema.set('toObject', { virtuals: true });
UserSchema.set('toJSON', { virtuals: true });

var User = mongoDB.hris.model(`users`, UserSchema, `users`);
module.exports.User = User;