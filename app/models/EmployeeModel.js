'use strict';
var {mongoDB} = require("./");
var table = 'employees';

mongoDB.set('useCreateIndex', true);
var Schema = mongoDB.Schema;
var EmployeeSchema = new Schema({
    fullname: {type: String, default: null},
    dob: {type: String, default: null},
    gender: {type: String, default: null},
    status_id: {type: Number},
    father_name: {type: String, default: null},
    nationality_id: {type: Number, default: null},
    employee_type_id: {type: Number, default: null},
    parent_id: {type: Schema.Types.ObjectId, default: null},
    pasport_number: {type: String, default: null},
    remaining_days_off: {type: Number, default: 0},
    address: {type: String, default: null},
    city: {type: String, default: null},
    country_id: {type: Number},
    hp: {type: String, default: null},
    phone_number: {type: String, default: null},
    email: {type: String, default: null},
    employee_code: {type: String, index: true, unique: true},
    departement_id: {type: Schema.Types.ObjectId},
    employee_status_id: {type: Number},
    join_date: {type: String, default: null},
    photo: {type: String, default: null},
    created_at: {type: String, default: null},
    created_by: {type: Schema.Types.ObjectId},
    updated_at: {type: String, default: null},
    updated_by: {type: Schema.Types.ObjectId}
});

EmployeeSchema.virtual('departement_info',{
    ref: 'departements',
    localField: 'departement_id',
    foreignField: '_id',
    justOne: true
});

EmployeeSchema.virtual('employee_status_info',{
    ref: 'employeeStatuses',
    localField: 'employee_status_id',
    foreignField: '_id',
    justOne: true
});

EmployeeSchema.virtual('status_info',{
    ref: 'statuses',
    localField: 'status_id',
    foreignField: '_id',
    justOne: true
});

EmployeeSchema.virtual('nationality_info',{
    ref: 'countries',
    localField: 'nationality_id',
    foreignField: '_id',
    justOne: true
});

EmployeeSchema.virtual('country_info',{
    ref: 'countries',
    localField: 'country_id',
    foreignField: '_id',
    justOne: true
});

EmployeeSchema.virtual('user_info',{
    ref: 'users',
    localField: '_id',
    foreignField: '_id',
    justOne: true
});

EmployeeSchema.virtual('employee_type_info',{
    ref: 'categories',
    localField: 'employee_type_id',
    foreignField: '_id',
    justOne: true
});

EmployeeSchema.set('toObject', { virtuals: true });
EmployeeSchema.set('toJSON', { virtuals: true });

var Employee = mongoDB.hris.model(table, EmployeeSchema, table);
module.exports.Employee = Employee;