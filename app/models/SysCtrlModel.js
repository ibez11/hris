'use strict';
var {mongoDB} = require("./");
var table = 'sysctrls';

function sysCtrlSchema(param){
    var Schema = mongoDB.Schema;
    var SysCtrlSchema = new Schema(param);
    
    SysCtrlSchema.set('toObject', { virtuals: true });
    SysCtrlSchema.set('toJSON', { virtuals: true });
    
    return mongoDB.hris.model(table, SysCtrlSchema, table);
}

var CurrentModel = sysCtrlSchema;
module.exports.SysCtrl = CurrentModel;
