'use strict';
var {mongoDB} = require("./");
var table = 'departements'

var Schema = mongoDB.Schema;
var DepartementSchema = new Schema({
    _id: {type: Schema.Types.ObjectId},
    name: String,
    description: String,
    created_at: String,
    created_by: {type: Schema.Types.ObjectId},
    updated_at: {type: String, default: null},
    updated_by: {type: Schema.Types.ObjectId}
});

var Departement = mongoDB.hris.model(table, DepartementSchema, table);
module.exports.Departement = Departement;