'use strict';
var {mongoDB} = require("./");

var Schema = mongoDB.Schema;
var RoleTypeSchema = new Schema({
    code: {type: Number},
    short_name: {type: String, default: ''},
    name: {type: String, default: ''},
});

var RoleType = mongoDB.hris.model(`roleTypes`, RoleTypeSchema, `roleTypes`);
module.exports.RoleType = RoleType;