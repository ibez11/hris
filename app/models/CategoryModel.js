'use strict';
var {mongoDB} = require("./");
var table = 'categories';

class Categories {
    constructor(){
        this.STATUS_DRAFT = 31;
        this.STATUS_PENDING = 30;
        this.STATUS_ACCEPTED = 28;
        this.STATUS_REJECTED = 29;

        this.EMPLOYEES_PERMANENT = 33;
        this.CONTRACT_EMPLOYEES = 34;
        this.EMPLOYEE_TRAINING = 35;
        this.RETIRED_EMPLOYEE = 36;
        this.RESIGN = 37;
    }
    getStatusIdByName(name)
    {
        let statusId = null;

        switch(name) {
            case 'draft':
                statusId = this.STATUS_DRAFT;
                break;

            case 'pending':
                statusId = this.STATUS_PENDING;
                break;

            case 'accepted':
                statusId = this.STATUS_ACCEPTED;
                break;

            case 'rejected':
                statusId = this.STATUS_REJECTED;
                break;
        }

        return statusId;
    }

    getEmployeeStatusIdByName(name)
    {
        let statusId = null;

        switch(name) {
            case 'permanent':
                statusId = this.EMPLOYEES_PERMANENT;
                break;

            case 'contract':
                statusId = this.CONTRACT_EMPLOYEES;
                break;

            case 'training':
                statusId = this.EMPLOYEE_TRAINING;
                break;

            case 'retired':
                statusId = this.RETIRED_EMPLOYEE;
                break;
            
            case 'resign':
                statusId = this.RESIGN;
                break;
        }

        return statusId;
    }
}

let categories = new Categories;
module.exports.Categories = categories;

var Schema = mongoDB.Schema;
var CategorySchema = new Schema({
    _id: {type: Number},
    category_id: {type: Number},
    name: {type: String, default: ''},
    label: {type: String, default: ''},
    notes: {type: String, default: ''},
    group_by: {type: String, default: ''},
    created_at: {type: String, default: ''},
    updated_at: {type: String, default: ''}
});

var Category = mongoDB.hris.model(table, CategorySchema, table);
module.exports.Category = Category;