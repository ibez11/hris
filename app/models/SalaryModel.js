'use strict';
var {mongoDB} = require("./");
var table = 'salaries';

mongoDB.set('useCreateIndex', true);
var Schema = mongoDB.Schema;
var SalarySchema = new Schema({
    employee_id: {type: Schema.Types.ObjectId},
    basic_salary: {type: Number, default: 0},
    positional_allowance: {type: Number, default: 0},
    fixed_allowance: {type: Number, default: 0},
    created_at: {type: String, default: null},
    created_by: {type: Schema.Types.ObjectId},
    updated_at: {type: String, default: null},
    updated_by: {type: Schema.Types.ObjectId}
});

SalarySchema.virtual('employee_info',{
    ref: 'employees',
    localField: 'employee_id',
    foreignField: '_id',
    justOne: true
});

SalarySchema.virtual('allowance_deduction_distribution_info',{
    ref: 'allowanceDeductionDistributions',
    localField: 'employee_id',
    foreignField: 'employee_id',
});

SalarySchema.set('toObject', { virtuals: true });
SalarySchema.set('toJSON', { virtuals: true });

var Salary = mongoDB.hris.model(table, SalarySchema, table);
module.exports.Salary = Salary;