'use strict';
var {mongoDB} = require("./");
var table = "remainingDaysOff"

var Schema = mongoDB.Schema;
var RemainingDaysOffSchema = new Schema({
    _id: {type: Number},
    category_id: {type: Number},
    count: {type: Number, default: 0},
});

var RemainingDaysOff = mongoDB.hris.model(table, RemainingDaysOffSchema, table);
module.exports.RemainingDaysOff = RemainingDaysOff;