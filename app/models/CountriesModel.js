'use strict';
var {mongoDB} = require("./");
var table = 'countries'

var Schema = mongoDB.Schema;
var CountriesSchema = new Schema({
    _id: {type: Number},
    name: {type: String, default: null}
});

var Countries = mongoDB.hris.model(table, CountriesSchema, table);
module.exports.Countries = Countries;