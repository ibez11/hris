'use strict';
var {mongoDB} = require("./");
var table = 'currentFinancialMonthYears'

var Schema = mongoDB.Schema;
var CurrentFinancialMonthYearSchema = new Schema({
    _id: {type: Schema.Types.ObjectId},
    month: String,
    year: String,
    is_selected: {type: Boolean, default: false},
    closed: {type: Boolean, default: true},
    created_at: String,
    created_by: {type: Schema.Types.ObjectId},
    updated_at: {type: String, default: null},
    updated_by: {type: Schema.Types.ObjectId}
});

var CurrentFinancialMonthYear = mongoDB.hris.model(table, CurrentFinancialMonthYearSchema, table);
module.exports.CurrentFinancialMonthYear = CurrentFinancialMonthYear;