const RandomString = require("../lib/Modules/RandomString");
const DateFormatter = require("../lib/Modules/DateFormatter");
const date = require('moment');
const {ObjectId} = require('mongodb'); // or ObjectID

var username = function(name) {
    const dateFormatter = new DateFormatter();
    
    var randomString = new RandomString();

    var nameCodeShortname = name.replace(/\s/g,'').substring(0, 6);
    
    if(nameCodeShortname.length < 6) {
        nameCodeShortname += randomString.makeString(6 - nameCodeShortname.length)
    }

    nameCodeShortname = nameCodeShortname.toUpperCase();

    let a = ( parseInt(dateFormatter.date(date)) + randomString.makeString(4) );
    let z = '';
    for(var i = 1; i <= (8 - parseInt(a.toString().length)); i++) {
        z+= Math.floor(Math.random() * 9);
    }

    let b = `${a}${z}`;

    result = `${nameCodeShortname}${b.substring(0, 14)}`;

    return result;
}

module.exports.username = username;

var password = function(name) {
    const dateFormatter = new DateFormatter();
    
    var randomString = new RandomString();

    var nameCodeShortname = randomString.makeString(8);
    
    result = `${nameCodeShortname}`;

    return result;
}

module.exports.password = password;