const pathLib = require('path');
const os = require('os');
const fs = require('fs')

require('dotenv').config();

class Upload {
    constructor(image) {
        this.image = this.uploadImage(image);
    }

    async uploadImage(image) {
        var result = [];
        
        try {
            var prom = await new Promise(async (resolve, reject) => {
                if (!image) {
                    reject({
                        status: false,
                        message: 'No image file.'});
                }
                
                var base64Img = "data:image/png;base64,AAA=";
                base64Img = base64Img.replace("data:image/png;base64,", "");
                
                var buf = Buffer.from(image.file.data, 'base64');
                var path = '../public/img/foto_karyawan/'
                var fileName = `the_badus_${Date.now()}.png`;
                fs.writeFile(`${path}${fileName}`, buf, 'binary', function(err) {
                    if(err)
                        reject({
                            status: false,
                            message: 'No image file'});

                    if(!err)
                        resolve({
                            status: true,
                            message: 'Telah berhasil tersimpan.',
                            filename: fileName})
                });
            });
        
            result['status'] = prom.status;
            result['message'] = prom.message;
            result['filename'] = prom.filename;
        
      } catch(e) {
        result['status'] = false;
        result['message'] = e.message;
      }
      
      return result;
    }
}

module.exports = Upload;