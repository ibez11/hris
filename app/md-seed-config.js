var mongoose = require("mongoose");
var Users = require('./database/seeds/users.seeder');
var Countries = require('./database/seeds/country.seeder');
var RoleTypes = require('./database/seeds/role-types.seeder');
var RolePermissions = require('./database/seeds/role-permissions.seeder');
var Categories = require('./database/seeds/categories.seeder');
var UserLoggeds = require('./database/seeds/user-loggeds.seeder');

const mongoURL = process.env.MONGO_URL || 'mongodb://localhost:27017/hris?authSource=admin';

/**
 * Seeders List
 * order is important
 * @type {Object}
 */
module.exports.seedersList = {
  Users,
  Categories,
  RoleTypes,
  RolePermissions,
  UserLoggeds,
  Countries
};
/**
 * Connect to mongodb implementation
 * @return {Promise}
 */
module.exports.connect = async () => {
  await mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true });
}
  
/**
 * Drop/Clear the database implementation
 * @return {Promise}
 */
module.exports.dropdb = async () => {
  mongoose.connection.db.dropDatabase();
} 
