var express = require("express");
var router = express.Router();

const { UserController } = require('../controllers/Api/UserController');
const { CountriesController } = require('../controllers/Api/CountriesController');
const { StatusesController } = require('../controllers/Api/StatusesController');
const { DepartementController } = require('../controllers/Api/DepartementController');
const { EmployeeController } = require('../controllers/Api/EmployeeController');
const { UploadImageController } = require('../controllers/Api/UploadImageController');
const { EmployeeStatusesController } = require('../controllers/Api/EmployeeStatusesController');
const { SalaryController } = require('../controllers/Api/SalaryController');
const { SalaryDescriptionController } = require('../controllers/Api/SalaryDescriptionController');
const { AnnouncementController } = require('../controllers/Api/AnnouncementController');
const { PaidLeaveController } = require('../controllers/Api/PaidLeaveController');
const { CategoryPaidLeaveController } = require('../controllers/Api/CategoryPaidLeaveController');
const { EmployeeTypeController } = require('../controllers/Api/EmployeeTypeController');
const { PaidLeaveRequestController } = require('../controllers/Api/PaidLeaveRequestController');
const { SettingController } = require('../controllers/Api/SettingController');
const { CurrentFinancialMonthYearController } = require('../controllers/Api/CurrentFinancialMonthYearController');

const { auth } = require('../controllers/AuthController');
const { Auth } = require('../middleware/Auth');

router.post('/login', auth.signIn);
router.post('/register', auth.register);

// Upload
router.post('/upload/image', Auth.middleware(['none']).isAuth, UploadImageController.upload);

// Profile
router.get('/profile', Auth.middleware(['none']).isAuth, UserController.profile);
router.put('/profile/update', Auth.middleware(['none']).isAuth, UserController.profile_update);

// Country
router.get('/master/country/list', Auth.middleware(['none']).isAuth, CountriesController.index);

// Statuses
router.get('/master/statuses/list', Auth.middleware(['none']).isAuth, StatusesController.index);

// Employee Statuses
router.get('/master/employee-statuses/list', Auth.middleware(['none']).isAuth, EmployeeStatusesController.index);

// Employee Type
router.get('/master/employee-type/list', Auth.middleware(['none']).isAuth, EmployeeTypeController.index);

// SalaryDescription Name
router.get('/master/salary-description/list', Auth.middleware(['none']).isAuth, SalaryDescriptionController.index);
router.get('/master/salary-description/detail/:id', Auth.middleware(['none']).isAuth, SalaryDescriptionController.show);

// Departement
router.get('/master/departement/list', Auth.middleware(['none']).isAuth, DepartementController.index);
router.get('/master/departement/detail/:id', Auth.middleware(['none']).isAuth, DepartementController.show);
router.post('/master/departement/create', Auth.middleware(['none']).isAuth, DepartementController.store);
router.delete('/master/departement/delete/:id', Auth.middleware(['none']).isAuth, DepartementController.destroy);

// Cuurent Financial Month Year
router.get('/master/currentfinancialmonthyear/list', Auth.middleware(['none']).isAuth, CurrentFinancialMonthYearController.index);
router.get('/master/currentfinancialmonthyear/detail/:id', Auth.middleware(['none']).isAuth, CurrentFinancialMonthYearController.show);
router.post('/master/currentfinancialmonthyear/setselected', Auth.middleware(['none']).isAuth, CurrentFinancialMonthYearController.setSelected);
router.post('/master/currentfinancialmonthyear/month-end', Auth.middleware(['none']).isAuth, CurrentFinancialMonthYearController.monthEnd);

// Employee
router.get('/master/employee/list', Auth.middleware(['none']).isAuth, EmployeeController.index);
router.get('/master/employee/detail/:id', Auth.middleware(['none']).isAuth, EmployeeController.show);
router.post('/master/employee/create', Auth.middleware(['none']).isAuth, EmployeeController.store);
router.delete('/master/employee/delete/:id', Auth.middleware(['none']).isAuth, EmployeeController.destroy);
router.get('/master/employee/generate', Auth.middleware(['none']).isAuth, EmployeeController.auto_generate);

// Salary
router.get('/master/salary/list', Auth.middleware(['none']).isAuth, SalaryController.index);
router.get('/master/salary/detail/:id', Auth.middleware(['none']).isAuth, SalaryController.show);
router.post('/master/salary/create', Auth.middleware(['none']).isAuth, SalaryController.store);
router.post('/master/salary/create_multiple', Auth.middleware(['none']).isAuth, SalaryController.storeMultipleEmployee);
router.delete('/master/salary/delete/:id', Auth.middleware(['none']).isAuth, SalaryController.destroy);
router.get('/master/salary/download/:id', Auth.middleware(['none']).isAuth, SalaryController.download);

// Announcement
router.get('/master/announcement/list', Auth.middleware(['none']).isAuth, AnnouncementController.index);
router.get('/master/announcement/detail/:id', Auth.middleware(['none']).isAuth, AnnouncementController.show);
router.post('/master/announcement/create', Auth.middleware(['none']).isAuth, AnnouncementController.store);
router.delete('/master/announcement/delete/:id', Auth.middleware(['none']).isAuth, AnnouncementController.destroy);
router.post('/master/announcement/publish', Auth.middleware(['none']).isAuth, AnnouncementController.publish);

// Category PaidLeave
router.get('/master/category-paid-leave/list', Auth.middleware(['none']).isAuth, CategoryPaidLeaveController.index);
router.get('/master/category-paid-leave/detail/:id', Auth.middleware(['none']).isAuth, CategoryPaidLeaveController.show);
router.post('/master/category-paid-leave/create', Auth.middleware(['none']).isAuth, CategoryPaidLeaveController.store);
router.delete('/master/category-paid-leave/delete/:id', Auth.middleware(['none']).isAuth, CategoryPaidLeaveController.destroy);

// PaidLeave
router.get('/master/paid-leave/list', Auth.middleware(['none']).isAuth, PaidLeaveController.index);
router.get('/master/paid-leave/detail/:id', Auth.middleware(['none']).isAuth, PaidLeaveController.show);
router.post('/master/paid-leave/create', Auth.middleware(['none']).isAuth, PaidLeaveController.store);
router.get('/master/paid-leave/download/:id', Auth.middleware(['none']).isAuth, PaidLeaveController.download);

// PaidLeave Request
router.get('/master/paid-leave-request/list', Auth.middleware(['none']).isAuth, PaidLeaveRequestController.index);
router.get('/master/paid-leave-request/detail/:id', Auth.middleware(['none']).isAuth, PaidLeaveRequestController.show);
router.get('/master/paid-leave-request/:id/set-status/:status', Auth.middleware(['none']).isAuth, PaidLeaveRequestController.setStatus);

// Setting
router.get('/setting', Auth.middleware(['none']).isAuth, SettingController.index);
router.get('/setting/:id', Auth.middleware(['none']).isAuth, SettingController.show);
router.post('/setting', Auth.middleware(['none']).isAuth, SettingController.store);


router.get("/", function(req,res) {
    res.status(500).send({
        success: false,
        message: "Maaf data tidak tersedia"
    })
});

module.exports = router;