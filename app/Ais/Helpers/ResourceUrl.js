
const getHost = () =>
{
    // Useful when you use multiple domain name

    return process.env.APP_URL+':'+process.env.APP_PORT;
}

const photo = (filename, path_from)  => 
{
    let path = ':host:/'+ path_from +'/:image-name:';
    path = path.replace(':host:', getHost());
    path = path.replace(':image-name:', filename);

    return path;
}


module.exports = photo;