var ValidationError = require('validation-error');

class AbstractRepository
{
    constructor(model) {
        this.model = model;
    }

    getModel()
    {
        return this.model;
    }

    async deleteOne(param)
    {
        await this.model.deleteOne(param, function(err) {
            if(err) {
                console.log('deleteOne', err)
            }
        });
    }

    async find(param)
    {
        return await this.model.find(param).exec();
    }

    async updateOne(condition, param)
    {
        await this.model.collection.updateOne(condition, param, { upsert: true, new: true }, function(err, doc) {
            if(err) {
                console.log('updateOne', err)
            };
        });
    }

    async validOrThrow(validator)
    {
        var data = [];
        await validator.check().then(function (matched) {
            data._error = {return: matched, result: validator.errors};
        });

        if(data._error.return == false) {
            var message = '';
            var error = Object.keys(data._error.result);
            
            error.forEach((v) => {
                message = data._error.result[v].message;
            });
            
            throw new ValidationError('message', message)
        }
    }

    async save()
    {
        await this.model.save(function(err) {
            if(err) {
                console.log('Save', err)
            }
        });
    }
}

module.exports = AbstractRepository;
