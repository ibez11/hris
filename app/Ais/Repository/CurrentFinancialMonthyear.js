const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID
const Validator = require('node-input-validator');

const AbstractRepository = require("./AbstractRepository");

const CurrentFinancialMonthYearModel = require("../../models/CurrentFinancialMonthYearModel").CurrentFinancialMonthYear;

class CurrentFinancialMonthYear extends AbstractRepository {
    constructor(model = new CurrentFinancialMonthYearModel()) {
        super(model);
    }

    async save() 
    {
        var result = [];

        mongoose.set('useFindAndModify', false);
        let params = this.model.toObject();

        let validator = null;
        
        var checkCountData = 0;
        
        if (!params._id)
        checkCountData = await CurrentFinancialMonthYearModel.countDocuments({month: params.month, year: params.year}).exec();

        Validator.addCustomMessages({
            'already_exist.in': 'Data sudah ada',
            'month.required': 'Bulan harus diisi',
            'year.required': 'Tahun harus diisi'
        });

        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });

        // Validation
        let fields = {
            already_exist: checkCountData == 0,
            month: params.month,
            year: params.year
        };

        let rules = {
            already_exist: 'in:true,false',
            month: 'required',
            year: 'required'
        };
        
        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);

        var currentId;
        if (params._id) {
            currentId = params._id;
            delete params._id;
        } else {
            currentId = ObjectId();
        }

        let data = {};
        var vm = this;
        return new Promise( function (fulfilled, rejected) {
            CurrentFinancialMonthYearModel.findOneAndUpdate({_id: currentId}, params, { upsert: true, new: true }, (err, doc) => {
                vm.model._id = currentId;
                if(err) {
                    rejected(err)
                }

                if(doc){
                    data = doc['_doc'];

                    result['status'] = 1;
                    result['message'] = 'Success';
                    result['data'] = data
                    
                    fulfilled(result)
                }
            });
        });
    }

    async changeAllIsSelectedFalse(param)
    {
        let params = this.model.toObject();
        await CurrentFinancialMonthYearModel.updateMany({_id: {$nin: [ObjectId(params._id)]}}, param).exec();
    }
}

module.exports = CurrentFinancialMonthYear;