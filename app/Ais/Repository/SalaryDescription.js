const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID

const AbstractRepository = require("./AbstractRepository");

const SalaryDescriptionModel = require("../../models/SalaryDescriptionModel").SalaryDescription;

class SalaryDescription extends AbstractRepository {
    constructor(model = new SalaryDescriptionModel()) {
        super(model);
    }

    async save() 
    {
        var result = [];
        
        try {
            mongoose.set('useFindAndModify', false);
            let param = this.model.toObject();
            
            var currentId;
            if (param._id) {
                currentId = param._id;
                delete param._id;
            } else {
                currentId = ObjectId();
            }

            let data = {};
            
            return new Promise( function (fulfilled, rejected) {
                SalaryDescriptionModel.findOneAndUpdate({_id: currentId}, param, { upsert: true, new: true }, (err, doc) => {
                    if(err) {
                        rejected(err)
                    }

                    if(doc){
                        data = doc['_doc'];

                        result['status'] = 1;
                        result['message'] = 'Success';
                        result['data'] = data
                        
                        fulfilled(result)
                    }
                    
                });
            });
        } catch(e) {
            console.log(e)
            result['status'] = 0;
            result['message'] = e.message;
            
        }
        
        return result;
    }
}

module.exports = SalaryDescription;