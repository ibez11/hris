const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID
var pdf = require("pdf-creator-node");
var fs = require('fs');
const DateFormatter = require("../../lib/Modules/DateFormatter");
const Validator = require('node-input-validator');

const NumberFormat = require("../../TheBadusLibs/Helper/NumberFormat");
const AbstractRepository = require("./AbstractRepository");
const Setting = require("./Setting");

const SalaryModel = require("../../models/SalaryModel").Salary;
const CurrentFinancialMonthYearModel = require("../../models/CurrentFinancialMonthYearModel").CurrentFinancialMonthYear;
const AllowanceDeductionDistributionModel = require("../../models/AllowanceDeductionDistributionModel").AllowanceDeductionDistribution;

class Salary extends AbstractRepository {
    constructor(model = new SalaryModel()) {
        super(model);
        this.allowanceDeductionDistributions = [];
    }

    addDetail(id, employeeId, itemId, name, code, amount, now)
    {
        this._addDetail(id, employeeId, itemId, name, code, amount, now);
    }

    _addDetail(id, employeeId, itemId, name, code, amount, now)
    {
        this.allowanceDeductionDistributions.push({
            id: id,
            employee_id: employeeId,
            salary_description_id: itemId,
            name: name,
            is_allowance: code,
            amount: amount,
            created_at: now
        });
    }

    async deleteDetail(id)
    {
        await AllowanceDeductionDistributionModel.deleteOne({_id: ObjectId(id), employee_id: ObjectId(this.model.employee_id)}).exec();
    }

    async save() 
    {
        var result = [];
        
        try {
            mongoose.set('useFindAndModify', false);
            let param = this.model.toObject();
            
            var currentId;
            if (param._id) {
                currentId = param._id;
                delete param._id;
            } else {
                currentId = ObjectId();
            }

            let data = {};
            let vm = this;
            return new Promise( function (fulfilled, rejected) {
                SalaryModel.findOneAndUpdate({_id: currentId}, param, { upsert: true, new: true }, (err, doc) => {
                    if(err) {
                        rejected(err)
                    }

                    if(doc) {
                        vm.saveDetail();
                        data = doc['_doc'];

                        result['status'] = 1;
                        result['message'] = 'Success';
                        result['data'] = data
                        
                        fulfilled(result)
                    }
                    
                });
            });
        } catch(e) {
            result['status'] = 0;
            result['message'] = e.message;
            
        }
        
        return result;
    }

    async validate()
    {
        let validator = null;
        
        var lastFinancial = await CurrentFinancialMonthYearModel.countDocuments({is_selected: true, closed: true}).exec();

        Validator.addCustomMessages({
            'last_financial.in': 'Anda belum tutup buku, Tidak bisa mencetak Pay Slip'
        });

        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });
        
        // Validation
        let fields = {
            last_financial:
                this.model.status_id == lastFinancial,
        };

        let rules = {
            last_financial: 'in:true,false'
        };
        
        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);
    }

    async saveMultiple()
    {
        this.validate();
        this.saveDetail();
    }

    async saveDetail()
    {
        try{
            mongoose.set('useFindAndModify', false);

            for(let i=0; i < this.allowanceDeductionDistributions.length; i++){
                var currentId;
                if (this.allowanceDeductionDistributions[i].id != null) {
                    currentId = this.allowanceDeductionDistributions[i].id;
                    
                    delete this.allowanceDeductionDistributions[i].id;
                    
                } else {
                    currentId = ObjectId();
                }

                let detailModel = await AllowanceDeductionDistributionModel.findOne({_id: currentId}).exec();

                if(!detailModel) {
                    detailModel = new AllowanceDeductionDistributionModel;
                } 

                detailModel.employee_id = this.allowanceDeductionDistributions[i].employee_id;
                detailModel.salary_description_id = this.allowanceDeductionDistributions[i].salary_description_id;
                detailModel.name = this.allowanceDeductionDistributions[i].name;
                detailModel.is_allowance = this.allowanceDeductionDistributions[i].is_allowance;
                detailModel.amount = this.allowanceDeductionDistributions[i].amount;
                detailModel.created_at = this.allowanceDeductionDistributions[i].created_at;
                
                delete detailModel['_doc']._id

                await AllowanceDeductionDistributionModel.findOneAndUpdate({_id: currentId}, detailModel, { upsert: true, new: true }, (err, doc) => {
                    if(err)
                    console.log(err)

                });
            }
            
        } catch(e) {
            console.log(e)
        }
    }

    async download(data, cookiesSelected)
    {
        let validator = null;
        var split = cookiesSelected.split("/");
        
        var lastFinancial = await CurrentFinancialMonthYearModel.countDocuments({month: split[1], year: split[1], closed: true}).exec();

        Validator.addCustomMessages({
            'last_financial.in': 'Anda belum tutup buku, Tidak bisa mencetak Pay Slip'
        });

        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });
        
        // Validation
        let fields = {
            last_financial:
                this.model.status_id == lastFinancial,
        };

        let rules = {
            last_financial: 'in:true,false'
        };
        
        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);

        var date = new Date().getTime();
        const dateFormatter = new DateFormatter();
        const now = dateFormatter.dateSlash(new Date());
        
        let model = await this.model;
        
        data = data.toObject();

        var allowances = [];
        var deductions = [];
        data.allowance_deduction_distribution_info.forEach(x => {
            if(x.is_allowance == true) {
                let dataAllowance = {
                    name: x.name,
                    amount: x.amount,
                }

                allowances.push(dataAllowance);
            } else {
                let dataDeduc = {
                    name: x.name,
                    amount: x.amount,
                }

                deductions.push(dataDeduc);
            }
        });

        data['allowances_deductions'] = [];
        var i = 0;
        var otherAllowance = 0;
        var otherDeduction = 0;
        var totalAllowance = 0;
        var totalDeduction = 0;
        while (data.allowance_deduction_distribution_info[i]) {
            totalAllowance += allowances[i] != undefined ?  allowances[i].amount : 0;
            totalDeduction += deductions[i] != undefined ?  deductions[i].amount : 0;
            var dataAllowanceDeduction = {};
            if(i <= 4) {
                dataAllowanceDeduction = {
                    allowances: { 
                        name: allowances[i] != undefined ?  allowances[i].name : '-',
                        amount: allowances[i] != undefined ?  NumberFormat.currency(allowances[i].amount, 'Rp') : ''
                    },
                    deductions: {
                        name: deductions[i] != undefined ? deductions[i].name : '-',
                        amount: deductions[i] != undefined ? NumberFormat.currency(deductions[i].amount, 'Rp') : ''
                    }
                }
                data['allowances_deductions'].push(dataAllowanceDeduction);
            }

            if(i > 4){
                otherAllowance += allowances[i] != undefined ?  allowances[i].amount : 0;
                otherDeduction += deductions[i] != undefined ?  deductions[i].amount : 0;
            }

            i++;
        }

        if(otherAllowance > 0 || otherDeduction > 0)
        data['allowances_deductions'].push({allowances : {name: 'Dan lain-lain', amount: NumberFormat.currency(otherAllowance, 'Rp')}, deductions: {name: 'Dan lain-lain', amount: NumberFormat.currency(otherDeduction, 'Rp')}});

        for(i=0; i < 2; i++) {
            data['allowances_deductions'].push({allowances : {name: '-', amount: ''}, deductions: {name: '-', amount: ''}});
        }

        var totalSalayBruto = totalAllowance + data.basic_salary + data.positional_allowance + data.fixed_allowance;
        var totalDeductionSalary = totalDeduction;
        var netSalary = totalSalayBruto + totalDeductionSalary;
        data['allowances_deductions'].push({allowances : {name: 'Jumlah kotor', amount: NumberFormat.currency((totalAllowance + data.basic_salary + data.positional_allowance + data.fixed_allowance), 'Rp')}, deductions: {name: 'Jumlah Potongan', amount: NumberFormat.currency(totalDeduction, 'Rp')}});
        data['net_salary'] = { name: 'Total Terima', amount: NumberFormat.currency(netSalary, 'Rp') };

        let setting = new Setting;

        data['Setting'] = {};

        data['Setting']['company_name'] = await setting.get('company_name');
        data['Setting']['company_tagline'] = await setting.get('company_tagline');
        data['Setting']['company_address'] = await setting.get('company_address');
        data['Setting']['company_phone'] = await setting.get('company_phone');

        var html = fs.readFileSync('views/report/pay-slip.ejs', 'utf8');
        var filename = 'gaji_' + model._id + '_' + date + '.pdf';

        data.basic_salary = NumberFormat.currency(data.basic_salary, 'Rp');
        data.positional_allowance = NumberFormat.currency(data.positional_allowance, 'Rp');
        data.fixed_allowance = NumberFormat.currency(data.fixed_allowance, 'Rp');
        data.deduc = 1;
        var document = {
            html: html,
            data: data,
            path: '../public/tmp/'+filename
        };

        var options = {
            format: "A3",
            orientation: "portrait",
            border: "10mm"
        };

        data['transaction_date'] = now;
        
        await pdf.create(document, options)
        .then(res => {
            // console.log(res)
        })
        .catch(error => {
            console.error(error)
        });

        return filename;
    }
}

module.exports = Salary;