const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID

const AbstractRepository = require("./AbstractRepository");

const AnnouncementModel = require("../../models/AnnouncementModel").Announcement;

class Announcement extends AbstractRepository {
    constructor(model = new AnnouncementModel()) {
        super(model);
        this.details = [];
        this.details.id = [];
    }

    async save() 
    {
        var result = [];
        
        try {
            mongoose.set('useFindAndModify', false);
            let params = this.model.toObject();
            
            var currentId;
            if (params._id) {
                currentId = params._id;
                delete params._id;
            } else {
                currentId = ObjectId();
            }

            let data = {};
            
            return new Promise( function (fulfilled, rejected) {
                AnnouncementModel.findOneAndUpdate({_id: currentId}, params, { upsert: true, new: true }, (err, doc) => {
                    
                    if(err) {
                        rejected(err)
                    }

                    if(doc){
                        data = doc['_doc'];

                        result['status'] = 1;
                        result['message'] = 'Success';
                        result['data'] = data
                        
                        fulfilled(result)
                    }
                    
                });
            });
        } catch(e) {
            console.log(e)
            result['status'] = 0;
            result['message'] = e.message;
            
        }
        
        return result;
    }

    addDetail(id, now, publishedBy)
    {
        this.details.id.push(id);
        this.details.updated = {is_published_at: now, is_published_by: publishedBy};
    }

    publish()
    {
        var result = [];
        this.details.id.forEach(async x => {
            await this.updateOne({_id: x}, { $set: {is_published: true, is_published_at: this.details.updated.is_published_at, is_published_by: this.details.updated.is_published_by}});
        })

        result['status'] = true;
        result['message'] = 'Telah berhasil di Publish';

        return result;
    }
}

module.exports = Announcement;