const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID

const AbstractRepository = require("./AbstractRepository");

const DepartementModel = require("../../models/DepartementModel").Departement;

class Departement extends AbstractRepository {
    constructor(model = new DepartementModel()) {
        super(model);
    }

    async save() 
    {
        var result = [];
        
        try {
            mongoose.set('useFindAndModify', false);
            let params = this.model.toObject();
            
            var currentId;
            if (params._id) {
                currentId = params._id;
                delete params._id;
            } else {
                currentId = ObjectId();
            }

            let data = {};
            
            return new Promise( function (fulfilled, rejected) {
                DepartementModel.findOneAndUpdate({_id: currentId}, params, { upsert: true, new: true }, (err, doc) => {
                    
                    if(err) {
                        rejected(err)
                    }

                    if(doc){
                        data = doc['_doc'];

                        result['status'] = 1;
                        result['message'] = 'Success';
                        result['data'] = data
                        
                        fulfilled(result)
                    }
                    
                });
            });
        } catch(e) {
            console.log(e)
            result['status'] = 0;
            result['message'] = e.message;
            
        }
        
        return result;
    }
}

module.exports = Departement;