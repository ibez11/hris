const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID
var pdf = require("pdf-creator-node");
var fs = require('fs');
const Validator = require('node-input-validator');

const AbstractRepository = require("./AbstractRepository");

const Category = require("../../models/CategoryModel").Categories;
const PaidLeaveModel = require("../../models/PaidLeaveModel").PaidLeave;

class PaidLeaveRequest extends AbstractRepository {
    constructor(model = new PaidLeaveModel()) {
        super(model);
    }

    
}

module.exports = PaidLeaveRequest;