const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID

const ModelReq = require("../../models/SysCtrlModel");

class Setting
{
    constructor(){
        this.list = [];
    }

    update(key, value)
    {
        this.list.push({
            key: key,
            value: value
        });
    }

    async get(key)
    {
        let value = null;

        const Model = ModelReq.SysCtrl({_id: String, [key.toLowerCase()]: {type: String}, value: {type: String}});
        let row = await Model.findOne({_id: key}).lean();
        
        if(row)
            value = row.value;

        return value;
    }

    async save()
    {
        let result = Promise.all(
            this.list.map(async x => {
                let key = x['key'];
                let value = x['value'];
                
                const Model = ModelReq.SysCtrl({_id: String, [key.toLowerCase()]: {type: String}, value: {type: String}});
                let row = await Model.findOne({_id: key}).exec();
                
                if(!row) {
                    row = new Model();
                    row._id = key.toLowerCase();
                    row.key = key;
                }
                
                row.value = value;
                row.save();

                return row;
            })
        )
        
        
        return result;
    }
}

module.exports = Setting;