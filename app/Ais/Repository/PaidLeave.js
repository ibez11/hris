const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID
var pdf = require("pdf-creator-node");
var fs = require('fs');
const Validator = require('node-input-validator');
const CountDays = require('../../lib/Modules/CountDays');

const AbstractRepository = require("./AbstractRepository");
const Setting = require("./Setting");

const Category = require("../../models/CategoryModel").Categories;
const Employee = require("../../models/EmployeeModel").Employee;
const PaidLeaveModel = require("../../models/PaidLeaveModel").PaidLeave;

class PaidLeave extends AbstractRepository {
    constructor(model = new PaidLeaveModel()) {
        super(model);
        this.employeeModel = null
    }

    reductionLeave(employee = new Employee())
    {
        this.employeeModel = employee;
    }

    async save() 
    {
        var result = [];
        
        mongoose.set('useFindAndModify', false);
        let param = this.model.toObject();
        
        var currentId;
        if (param._id) {
            currentId = param._id;
            delete param._id;
        } else {
            currentId = ObjectId();
        }

        let data = {};
        
        return new Promise( function (fulfilled, rejected) {
            PaidLeaveModel.findOneAndUpdate({_id: currentId}, param, { upsert: true, new: true }, (err, doc) => {
                if(err) {
                    rejected(err)
                }

                if(doc){
                    data = doc['_doc'];

                    result['status'] = 1;
                    result['message'] = 'Success';
                    result['data'] = data
                    
                    fulfilled(result)
                }
                
            });
        });
    }

    async setStatus(statusId)
    {
        let validator = null;
        var remainingLeave = await Employee.findOne({_id: this.model.employee_id}).lean();

        Validator.addCustomMessages({
            'draft_to_pending.in': 'Gagal Ubah Status',
            'remaining_leave.in': 'Sisa cuti anda melebihi dari pengajuan'
        });

        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });

        if(statusId == Category.STATUS_PENDING) {
            // Validation
            let fields = {
                draft_to_pending:
                    this.model.status_id == Category.STATUS_DRAFT &&
                    statusId == Category.STATUS_PENDING,
                remaining_leave: CountDays({start_date: this.model.start_date, end_date: this.model.end_date}) <= remainingLeave.remaining_days_off
            };

            let rules = {
                draft_to_pending: 'in:true,false',
                remaining_leave: 'in:true,false'
            };
            
            validator = new Validator.Validator( fields, rules );
            await this.validOrThrow(validator);
        }
        
        this.model.status_id = statusId;
        await this.model.save(function(err, doc) {
            if(err)
                console.log(err)
        });
    }

    async download(data)
    {
        var date = new Date().getTime();
        
        let model = await this.model;
        
        const oneDay = 24 * 60 * 60 * 1000;
        const firstDate = new Date(String(data.start_date).split('-')[0], String(data.start_date).split('-')[1], String(data.start_date).split('-')[2]);
        const secondDate = new Date(String(data.end_date).split('-')[0], String(data.end_date).split('-')[1], String(data.end_date).split('-')[2]);

        const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
        data = data.toObject();

        let setting = new Setting;

        data['Setting'] = {};

        data['Setting']['company_name'] = await setting.get('company_name');
        data['Setting']['company_tagline'] = await setting.get('company_tagline');
        data['Setting']['company_address'] = await setting.get('company_address');
        data['Setting']['company_phone'] = await setting.get('company_phone');

        data.count_day = diffDays;
        
        var html = fs.readFileSync('views/report/paid-leave.ejs', 'utf8');
        var filename = 'cuti_' + model._id + '_' + date + '.pdf';

        var document = {
            html: html,
            data: data,
            path: '../public/tmp/'+filename
        };

        var options = {
            format: "A3",
            orientation: "portrait",
            border: "10mm"
        };
        
        await pdf.create(document, options)
        .then(res => {
            // console.log(res)
        })
        .catch(error => {
            console.error(error)
        });

        return filename;
    }

    async reductionLeaveSave(reductionLeaveSave)
    {
        this.employeeModel.remaining_days_off = reductionLeaveSave;
        
        this.employeeModel.save();
    }
}

module.exports = PaidLeave;