const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID

const AbstractRepository = require("./AbstractRepository");

const EmployeeModel = require("../../models/EmployeeModel").Employee;
const UserModel = require("../../models/UserModel").User;

class Employee extends AbstractRepository {
    constructor(model = new EmployeeModel()) {
        super(model);
        this.userModel = null
    }

    addUser(user = new UserModel())
    {
        this.userModel = user;
    }

    async save() 
    {
        var result = [];
        
        try {
            mongoose.set('useFindAndModify', false);
            let param = this.model.toObject();
            let user = this.userModel.toObject();
            
            var currentId;
            if (param._id) {
                currentId = param._id;
                delete param._id;
                delete user._id;
            } else {
                currentId = ObjectId();
            }

            let data = {};
            
            return new Promise( function (fulfilled, rejected) {
                EmployeeModel.findOneAndUpdate({_id: currentId}, param, { upsert: true, new: true }, (err, doc) => {
                    if(err) {
                        rejected(err)
                    }

                    if(doc){
                        UserModel.findOneAndUpdate({_id: currentId}, user, { upsert: true, new: true }, (err1, doc1) => {
                            if(err)
                            console.log(err1)
                        });
                        data = doc['_doc'];

                        result['status'] = 1;
                        result['message'] = 'Success';
                        result['data'] = data
                        
                        fulfilled(result)
                    }
                    
                });
            });
        } catch(e) {
            result['status'] = 0;
            result['message'] = e.message;
            
        }
        
        return result;
    }

    async getHierarchy(param)
    {
        var descendants=[];
        var stack=[];
        var list=[];
        
        stack.push(param);
        while (stack.length>0){
            var currentnode = await stack.pop();
            var children = await EmployeeModel.collection.find({parent_id: currentnode._id});
            while(true === await children.hasNext()) {
                var child = await children.next();
                
                descendants.push(child._id)
                stack.push(child);
            }
        }
        
        list = {$in: descendants};

        return list;
    }

    async getSubordinate()
    {
        var list=[];
        
        var subordinates = await EmployeeModel.find({parent_id: this.model._id}).exec();
        
        subordinates.forEach(x => {
            list.push(x._id);
        })
        
        return {$in: list};
    }
}

module.exports = Employee;