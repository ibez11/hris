"use strict";
const SalaryModel = require("../../../models/SalaryModel").Salary;
require('dotenv').config();

class SalaryFinder {
    
    constructor(param) {
        this.page = 1;
        this.per_page = 15;
        this.param = param;
        this.where = {};
        this.pipeline = [
        {
            $lookup: {
                from: 'employees',
                localField: 'employee_id',
                foreignField: '_id',
                as: 'employee_info'
            }
        },
        {
            $lookup: {
                from: 'departements',
                localField: 'employee_info.departement_id',
                foreignField: '_id',
                as: 'departement_info'
            }
        },
        {
            $lookup: {
                from: 'categories',
                localField: 'employee_info.employee_status_id',
                foreignField: '_id',
                as: 'employee_status_info'
            }
        },
        {
            $lookup: {
                from: 'categories',
                localField: 'employee_info.status_id',
                foreignField: '_id',
                as: 'status_info'
            }
        },
        {
            $lookup: {
                from: 'countries',
                localField: 'employee_info.nationality_id',
                foreignField: '_id',
                as: 'nationality_info'
            }
        },
        {
            $lookup: {
                from: 'countries',
                localField: 'employee_info.country_id',
                foreignField: '_id',
                as: 'country_info'
            }
        }
    ];
        
        this.query = SalaryModel;
    }

    async orderBy(columnName, orderBy)
    {
        switch(columnName) {
            case 'departement_info.name':
                this.pipeline.push({$sort: { 'departement_info.name': orderBy == 'desc' ? -1 : 1 }});
                break;
            case 'employee_status_info.name':
                this.pipeline.push({$sort: { 'employee_status_info.name': orderBy == 'asc' ? -1 : 1 }});
                break;
            case 'employee_info.fullname':
                this.pipeline.push({$sort: { fullname: orderBy == 'asc' ? -1 : 1 }});
                break;
            case 'employee_info.employee_id':
                this.pipeline.push({$sort: { fullname: orderBy == 'asc' ? -1 : 1 }});
                break;
            case 'basic_salary':
                this.pipeline.push({$sort: { basic_salary: orderBy == 'asc' ? -1 : 1 }});
                break;
            default:
                this.pipeline.push({$sort: { created_at : -1 }});
                break;
        }
    }

    setPerPage(per_page)
    {
        this.per_page = per_page;
    }

    getPerPage()
    {
        return this.per_page;
    }

    async setPage(page)
    {
        this.page = page;
    }

    async getPage()
    {
        return this.page;
    }

    setKeyword(keyword)
    {
        if(keyword) {
            let query = [];
            // Split keyword first
            let listKeyword = keyword.split(" ");
            listKeyword = listKeyword.map(function(elem){
                return elem.trim();
            });;

            let columnList = [];
            let pattern = '';
            listKeyword.forEach(keyword => {
                pattern = `.*${keyword}.*`;
                columnList.push('employee_info.fullname', 
                'employee_info.father_name', 
                'employee_info.phone_number',
                'employee_info.employee_id', 
                'departement_info.name',
                'employee_status_info.name'
                );
            })

            columnList.forEach(x => {
                query.push(
                    { [x]: { $regex: pattern, $options: 'si'} }
                );
            })
            
            this.pipeline.push({$match: {$or: query}});
            this.where = {$or: query};
        }
    }

    async get()
    {
        let where = {};
        where = this.where;
        
        let query = this.query;
        let pipeline = this.pipeline;
        
        switch(this.page) {
            case 'all':
                return new Promise( async function (fulfilled, rejected) {
                    SalaryModel.countDocuments(where,function(err,count){
                        query.aggregate(pipeline).exec(function(err, docs) {
                            if (err) {
                                console.log(err)
                            } else {
                                var data = {
                                    data: docs,
                                    total: count,
                                    total_page: 1
                                }
                                
                                fulfilled(data)
                            }
                        });
                    });
                });
            default:
                let page = parseInt(this.page);
                let perPage = parseInt(this.per_page);
                
                return new Promise( async function (fulfilled, rejected) {
                    SalaryModel.countDocuments(where,function(err,count){
                        pipeline.push({$limit: perPage});
                        pipeline.push({
                            $skip: page > 0 ? ((page - 1) * perPage) : 0
                        });
                        
                        query.aggregate(pipeline).exec(function(err, docs) {
                            
                            if (err) {
                                console.log(err)
                            } else {
                                var data = {
                                    data: docs,
                                    current_page: page,
                                    last_page: Math.ceil(parseInt(count)/perPage),
                                    per_page: perPage,
                                    total: count,
                                    total_page: Math.ceil(parseInt(count)/perPage)
                                }
                                
                                fulfilled(data)
                            }
                        });
                    });
                });
        }
    }
}

module.exports = SalaryFinder;