"use strict";
const RemainingDaysOffModel = require("../../../models/RemainingDaysOffModel").RemainingDaysOff;
require('dotenv').config();

class RemainingDaysOffFinder {
    
    constructor(param) {
        this.page = 1;
        this.per_page = 15;
        this.param = param;
        this.where = {};
        this.pipeline = [
            {
                $lookup: {
                    from: 'categories',
                    localField: 'category_id',
                    foreignField: '_id',
                    as: 'employee_type'
                }
            },];
        
        this.query = RemainingDaysOffModel;
    }

    async orderBy(columnName, orderBy)
    {
        switch(columnName) {
            case 'employee_type.label':
                this.pipeline.push({$sort: { 'employee_type.label': orderBy == 'desc' ? -1 : 1 }});
                break;
            case 'name':
                this.pipeline.push({$sort: { 'name': orderBy == 'asc' ? -1 : 1 }});
                break;
            default:
                this.pipeline.push({$sort: { created_at : -1 }});
                break;
        }
}

    setPerPage(per_page)
    {
        this.per_page = per_page;
    }

    getPerPage()
    {
        return this.per_page;
    }

    async setPage(page)
    {
        this.page = page;
    }

    async getPage()
    {
        return this.page;
    }

    setKeyword(keyword)
    {
        if(keyword) {
            let query = [];
            // Split keyword first
            let listKeyword = keyword.split(" ");
            listKeyword = listKeyword.map(function(elem){
                return elem.trim();
            });;

            let columnList = [];
            let pattern = '';
            listKeyword.forEach(keyword => {
                pattern = `.*${keyword}.*`;
                columnList.push(
                    'name',
                    'categories.label',
                    );
            })

            columnList.forEach(x => {
                query.push(
                    { [x]: { $regex: pattern, $options: 'si'} }
                );
            })
            
            this.pipeline.push({$match: {$or: query}});
            this.where = {$or: query};
        }
    }

    async get()
    {
        let where = {};
        where = this.where;
        
        let query = this.query;
        let pipeline = this.pipeline;
        
        switch(this.page) {
            case 'all':
                return new Promise( async function (fulfilled, rejected) {
                    RemainingDaysOffModel.countDocuments(where,function(err,count){
                        query.aggregate(pipeline).exec(function(err, docs) {
                            if (err) {
                                console.log(err)
                            } else {
                                var data = {
                                    data: docs,
                                    total: count,
                                    total_page: 1
                                }
                                
                                fulfilled(data)
                            }
                        });
                    });
                });
            default:
                let page = parseInt(this.page);
                let perPage = parseInt(this.per_page);
                
                return new Promise( async function (fulfilled, rejected) {
                    RemainingDaysOffModel.countDocuments(where,function(err,count){
                        pipeline.push({$limit: perPage});
                        pipeline.push({
                            $skip: page > 0 ? ((page - 1) * perPage) : 0
                        });
                        
                        query.aggregate(pipeline).exec(function(err, docs) {
                            
                            if (err) {
                                console.log(err)
                            } else {
                                var data = {
                                    data: docs,
                                    current_page: page,
                                    last_page: Math.ceil(parseInt(count)/perPage),
                                    per_page: perPage,
                                    total: count,
                                    total_page: Math.ceil(parseInt(count)/perPage)
                                }
                                
                                fulfilled(data)
                            }
                        });
                    });
                });
        }
    }
}

module.exports = RemainingDaysOffFinder;