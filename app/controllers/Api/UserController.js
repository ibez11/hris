const ApiController = require('./ApiController');
const {ObjectId} = require('mongodb'); // or ObjectID
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');

const UserModel = require("../../models/UserModel").User;

const User = require("../../Ais/Repository/User");
const RoleType = require("../../Ais/Repository/RoleType");
const PasswordGenerate = require("../../TheBadusLibs/ThePasswordGenerate");

class UserController extends ApiController
{
    controller()
    {
        return {
            profile: async (req,res) => 
            {
                let repoRoleType = new RoleType();
                try {
                    let result = req.user_info;
                    let permission = await this.getAccessControl(req.user.user_id)
                    
                    result['permission'] = await permission.permission;
                    result['role_type_name'] = await repoRoleType.getRoleTypeNameByCode(result.role_type);

                    this.jsonResponse.setData(result);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(result));
                    res.status(200).send(this.jsonResponse.getResponse())
                } catch(e) {
                    console.log(e)
                    res.status(200).send({
                        success: false,
                        message: 'Failed'
                    })
                }
            },
            profile_update: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                
                try {
                    let model = new UserModel();
                    
                    model._id = req.body.id
                    model.full_name = req.body.full_name;
                    model.gender = req.body.gender;
                    model.email = req.body.email;
                    model.address = req.body.address;
                    model.phone = req.body.phone;
                    model.password = req.body.password == req.body.current_password ? req.body.current_password : PasswordGenerate(req.body.password);
                    model.is_actived = true;
                    model.approved = true;
                    model.updated_at = req.body.id ? now : undefined;
                    model.updated_by = req.body.id ? ObjectId(req.user.user_id) : undefined;

                    if(!req.body.id)
                        delete model['_doc']._id;
                    
                    let repoUser = new User(model);
                    let result = await repoUser.profile_update();
                    
                    this.jsonResponse.setMessage(`${result['data'].full_name}  telah berhasil tersimpan.`);
                    this.jsonResponse.setData(result['data']._id);

                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse());

                } catch(e) {
                    this.jsonResponse.setMessage('Failed');
                    this.jsonResponse.setError(true);

                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse());
                }
            }
        }
    }
}

let user = new UserController();
module.exports.UserController = user.controller();