const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');
const qs = require('qs');

const CurrentFinancialMonthYear = require("../../Ais/Repository/CurrentFinancialMonthYear");

const CurrentFinancialMonthYearModel = require("../../models/CurrentFinancialMonthYearModel").CurrentFinancialMonthYear;

const CurrentFinancialMonthYearFinder = require("../../Ais/Repository/Finder/CurrentFinancialMonthYearFinder");

class CurrentFinancialMonthYearController extends ApiController
{
    async getModel(id)
    {
        let row = await CurrentFinancialMonthYearModel.findOne({_id: id}).exec();

        if(!row)
            throw new Error('CurrentFinancialMonthYear tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new CurrentFinancialMonthYearFinder(req.query);
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {    
                        finder.orderBy('created_at', 'desc');
                    }
                    
                    var paginator = await finder.get();
                    
                    this.jsonResponse.setData(paginator.data);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    
                    let result = await this.getModel(id);
                    
                    this.jsonResponse.setData(result);
                    this.jsonResponse.setMessage('Success');
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }
                
                return res.status(200).send(this.jsonResponse.getResponse());
            },
            setSelected: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    req = qs.parse(req);
                    
                    let model = new CurrentFinancialMonthYearModel;
        
                    // Get Id
                    let id = req.body._id;
                    
                    if(id){
                        model = await CurrentFinancialMonthYearModel.findOne({_id: id}).exec();
                    }

                    let repoCurrentFinancialMonthYear = new CurrentFinancialMonthYear(model);

                    model.is_selected = req.body.is_selected;
                    
                    let result = await repoCurrentFinancialMonthYear.save();

                    if(req.body.is_selected == 'true') {
                        await repoCurrentFinancialMonthYear.changeAllIsSelectedFalse({is_selected: false});
                    }
                    
                    this.jsonResponse.setMessage(`Data telah berhasil tersimpan.`);
                    this.jsonResponse.setData(result['data']._id);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            monthEnd: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new CurrentFinancialMonthYearModel;
                    var checkLastTransaction = await CurrentFinancialMonthYearModel.findOne({}).sort({_id:-1}).limit(1);

                    let repo = new CurrentFinancialMonthYear(model);
                    
                    if(checkLastTransaction) {
                        model.closed = false;
                        model.is_selected = true;
                        model.month = parseFloat(checkLastTransaction.month) < 11 ? parseFloat(checkLastTransaction.month) + 1 : 0;
                        model.year = parseFloat(checkLastTransaction.month) < 11 ? parseFloat(checkLastTransaction.year) : (parseFloat(checkLastTransaction.year) + 1);
                    } else {
                        model.closed = false;
                        model.is_selected = true;
                        model.month = parseFloat(dateFormatter.dateCustom(new Date(), "MM")) - 1;
                        model.year = dateFormatter.dateCustom(new Date(), "YYYY");
                    }
                    

                    model.created_at = now;
                    model.created_by = ObjectId(req.user.user_id);

                    let result = await repo.save();

                    await repo.changeAllIsSelectedFalse({is_selected: false, closed: true});

                    this.jsonResponse.setMessage(`Anda berhasil tutup buku bulan ini.`);
                    this.jsonResponse.setData(result['data']._id);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let departement = new CurrentFinancialMonthYearController();
module.exports.CurrentFinancialMonthYearController = departement.controller();