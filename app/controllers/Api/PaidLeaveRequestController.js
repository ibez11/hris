const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');
const CountDays = require('../../lib/Modules/CountDays');
const qs = require('qs');

const PaidLeave = require("../../Ais/Repository/PaidLeave");
const Employee = require("../../Ais/Repository/Employee");

const PaidLeaveModel = require("../../models/PaidLeaveModel").PaidLeave;
const Status = require("../../models/CategoryModel").Categories;
const CategoryModel = require("../../models/CategoryModel").Category;
const EmployeeModel = require("../../models/EmployeeModel").Employee;

const PaidLeaveRequestFinder = require("../../Ais/Repository/Finder/PaidLeaveRequestFinder");

class PaidLeaveRequestController extends ApiController
{
    async getModel(id)
    {
        let row = await PaidLeaveModel.findOne({_id: id}).populate({
            path: 'status_info',
            model: CategoryModel
        })
        .populate({
            path: 'employee_info',
            model: EmployeeModel,
            populate: [{
                path: 'employee_type_info'
            }]
        })
        .exec();

        if(!row)
            throw new Error('Permintaan Cuti tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new PaidLeaveRequestFinder(req.query);
                this.jsonResponse = this.jResp();
                try {
                    var employee = await EmployeeModel.findOne({_id: req.user.user_id}).lean();

                    var repoEmployee = new Employee(employee);

                    let subordinates = await repoEmployee.getSubordinate();
                    
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {
                        finder.orderBy('created_at', -1);
                    }

                    if(req.query.status) {
                        let statusId = Status.getStatusIdByName(req.query.status == 'draft' ? 'pending' : req.query.status);
                        finder.setStatusId(statusId);
                    } else {
                        let statusId = Status.getStatusIdByName('draft');
                        finder.setStatusFromHeadById(statusId);
                    }

                    finder.setEmployeeIn(subordinates);
                    
                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        list.push({
                            fullname: x.employee_info.length ? x.employee_info[0].fullname : '',
                            status_info: x.status_info.length ? x.status_info[0].label : '',
                            employee_status_info: x.employee_status_info.length ? x.employee_status_info[0].label : '',
                            email: x.employee_info.length ? x.employee_info[0].email : '',
                            join_date: x.employee_info.length ? x.employee_info[0].join_date : '',
                            photo: x.employee_info.length ? x.employee_info[0].photo : '',
                            created_at: x.created_at,
                            updated_at: x.updated_at,
                            _id: x._id,
                            created_by: x.created_by,
                            employee_id: x.employee_info.length ? x.employee_info[0].employee_id : '',
                            departement_info: x.departement_info.length ? x.departement_info[0].name : '',
                            start_date: x.start_date,
                            end_date: x.end_date,
                            status: x.status_paid_leave,
                            category_paid_leave_info: x.category_paid_leave_info,
                            date_of_filing: x.date_of_filing
                        });
                    })

                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    
                    let result = await this.getModel(id);
                    let list = {
                        _id: result._id,
                        category_leave_id: String(result.category_leave_id),
                        start_date: result.start_date,
                        end_date: result.end_date,
                        notes: result.notes,
                        status: result.status_info,
                        status_id: String(result.status_id),
                        created_at: result.created_at,
                        created_by: result.created_by,
                        updated_at: result.updated_at,
                        updated_by: result.updated_by
                    };

                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMessage('Success');
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            setStatus: async (req,res) => 
            {
                this.jsonResponse = this.jResp();

                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                try {
                    let id = req.params.id;
                    let status = req.params.status;

                    var message = null;
                    var inArray = ['accepted', 'revision', 'draft', 'pending'];
                    // If status is not accepted or decline
                    if(!inArray.includes(status)) {
                        throw new Error('Cuti tidak ditemukan');
                    }
                    
                    // Get status id
                    var statusId = Status.getStatusIdByName(status);

                    // Get quotation model
                    var model = await this.getModel(id);
                    var modelEmployee = await EmployeeModel.findOne({_id: model.employee_info._id}).exec();

                    // Get quotation repository
                    var repo = new PaidLeave(model);

                    model.is_status_changed_at = now;
                    model.is_status_changed_by = req.user.user_id;

                    repo.reductionLeave(modelEmployee);
                    await repo.setStatus(statusId);

                    switch(statusId) {
                        case Status.STATUS_ACCEPTED:
                            repo.reductionLeaveSave(model.employee_info.remaining_days_off - CountDays({start_date: model.start_date, end_date: model.end_date}));
                            message = 'Cuti berhasil disetujui';
                            break;
                        case Status.STATUS_REJECTED:
                            message = 'Cuti berhasil ditolak';
                            break;
                        case Status.STATUS_DRAFT:
                            message = 'Cuti berhasil disimpan';
                            break;
                        case Status.STATUS_PENDING:
                            message = 'Cuti berhasil ditunda';
                            break;
                    }
                    
                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }
                
                return res.status(200).send(this.jsonResponse.getResponse());
            }
        }
    }
}

let employee = new PaidLeaveRequestController();
module.exports.PaidLeaveRequestController = employee.controller();