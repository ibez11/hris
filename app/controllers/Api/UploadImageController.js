const ApiController = require('./ApiController');
const photo = require("../../Ais/Helpers/ResourceUrl");

class UploadImageController extends ApiController
{
    controller()
    {
        return {
            upload: async (req,res) => 
            {
                try {
                    let result = this.upload(req.files);
                    result = await result.image;
                    
                    if(result.status) {
                        this.jsonResponse.setData({filename: result.filename, url: photo(result.filename, 'img/foto_karyawan')});
                        this.jsonResponse.setMessage(`${result.filename} ${result.message}`);
                    } else {
                        this.jsonResponse.setError(true);
                        this.jsonResponse.setMessage(`${result.message}`);
                    }
                    
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let UploadImage = new UploadImageController();
module.exports.UploadImageController = UploadImage.controller();