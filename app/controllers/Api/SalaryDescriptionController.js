const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');
const { username } = require("../../TheBadusLibs/Generate");
const { password } = require("../../TheBadusLibs/Generate");

const SalaryDescription = require("../../Ais/Repository/SalaryDescription");

const SalaryDescriptionModel = require("../../models/SalaryDescriptionModel").SalaryDescription;

const SalaryDescriptionFinder = require("../../Ais/Repository/Finder/SalaryDescriptionFinder");

class SalaryDescriptionController extends ApiController
{
    async getModel(id)
    {
        let row = await SalaryDescriptionModel.findOne({_id: id}).exec();

        if(!row)
            throw new Error('SalaryDescription tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new SalaryDescriptionFinder(req.query);
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {
                        finder.orderBy('created_at', -1);
                    }
                    
                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        list.push({
                            _id: x._id,
                            name: x.name,
                            code: x.code,
                            description: x.description
                        });
                    })

                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    console.log(e)
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    
                    let result = await this.getModel(id);
                    
                    let list = {
                        _id: result._id,
                        name: result.name,
                        code: result.code,
                        description: result.description
                    };

                    this.jsonResponse.setData(list);

                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse())
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                    
                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse());
                }
            },
            store: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new SalaryDescriptionModel;

                    // Get Id
                    let id = req.body._id;
                    
                    if(id) {
                        model = await SalaryDescriptionModel.findOne({_id: id}).exec();
                    }

                    let repoSalaryDescription = new SalaryDescription(model);
                    
                    model.basic_salary = req.body.basic_salary;
                    model.positional_SalaryDescription = req.body.positional_SalaryDescription;
                    model.fixed_SalaryDescription = req.body.fixed_SalaryDescription;
                    
                    model.created_at = !id ? now : undefined;
                    model.created_by = !id ? ObjectId(req.user.user_id) : undefined;
                    model.updated_at = id ? now : undefined;
                    model.updated_by = id ? ObjectId(req.user.user_id) : undefined;

                    let result = await repoSalaryDescription.save();

                    this.jsonResponse.setMessage(`${result['data'].fullname}  telah berhasil tersimpan.`);
                    this.jsonResponse.setData(result['data']._id);

                } catch(e) {
                    console.log(e)
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            destroy: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;
                    
                    let row = await this.getModel(id);
                    let repo = new SalaryDescription(row);
                    await repo.deleteOne({_id: id});
                    
                    let message = `${row.name} berhasil dihapus`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            auto_generate: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    
                    let gen = 'u';
                    if(req.query.key == 'username') {
                        gen = username(req.query.string);
                    } else {
                        gen = password(req.query.string);
                    }
                    

                    let message = `Success`;
                    this.jsonResponse.setData({gen: gen})
                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let employee = new SalaryDescriptionController();
module.exports.SalaryDescriptionController = employee.controller();