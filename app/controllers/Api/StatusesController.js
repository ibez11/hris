const ApiController = require('./ApiController');


const CategoryModel = require("../../models/CategoryModel").Category;

const StatusesFinder = require("../../Ais/Repository/Finder/StatusesFinder");

class StatusesController extends ApiController
{
    async getModel(id)
    {
        let row = await StatusesModel.findOne({_id: id}).exec();

        if(!row)
            throw new Error('Statuses tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new StatusesFinder(req.query);
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {    
                        finder.orderBy('created_at', 'desc');
                    }
                    
                    var paginator = await finder.get();
                    
                    this.jsonResponse.setData(paginator.data);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            }
        }
    }
}

let Statuses = new StatusesController();
module.exports.StatusesController = Statuses.controller();