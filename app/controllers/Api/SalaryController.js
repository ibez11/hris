const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');
const { username } = require("../../TheBadusLibs/Generate");
const { password } = require("../../TheBadusLibs/Generate");
const qs = require('qs');

const Salary = require("../../Ais/Repository/Salary");

const SalaryModel = require("../../models/SalaryModel").Salary;

const SalaryFinder = require("../../Ais/Repository/Finder/SalaryFinder");

class SalaryController extends ApiController
{
    async getModel(id, selected)
    {
        let row = await SalaryModel.findOne({_id: id}).populate({
            path: 'allowance_deduction_distribution_info',
            match: {created_at: { '$regex': selected, '$options' : 'ig'}}
        }).populate({
            path: 'employee_info',
            populate: {
                path: 'employee_type_info'
            }
        }).exec();

        if(!row)
            throw new Error('Salary tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new SalaryFinder(req.query);
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {
                        finder.orderBy('created_at', -1);
                    }
                    
                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        list.push({
                            fullname: x.employee_info.length ? x.employee_info[0].fullname : '',
                            dob: x.employee_info.length ? x.employee_info[0].dob : '',
                            gender: x.employee_info.length ? x.employee_info[0].gender : '',
                            status_info: x.status_info.length ? x.status_info[0].label : '',
                            father_name: x.employee_info.length ? x.employee_info[0].father_name : '',
                            nationality_info: x.nationality_info.length ? x.nationality_info[0].name : '',
                            pasport_number: x.employee_info.length ? x.employee_info[0].pasport_number : '',
                            address: x.employee_info.length ? x.employee_info[0].address : '',
                            city: x.employee_info.length ? x.employee_info[0].city : '',
                            country: x.country_info.length ? x.country_info.name : '',
                            hp: x.employee_info.length ? x.employee_info[0].hp : '',
                            phone_number: x.employee_info.length ? x.employee_info[0].phone_number : '',
                            employee_status_info: x.employee_status_info.length ? x.employee_status_info[0].label : '',
                            email: x.employee_info.length ? x.employee_info[0].email : '',
                            join_date: x.employee_info.length ? x.employee_info[0].join_date : '',
                            photo: x.employee_info.length ? x.employee_info[0].photo : '',
                            created_at: x.created_at,
                            updated_at: x.updated_at,
                            _id: x._id,
                            created_by: x.created_by,
                            employee_id: x.employee_info.length ? x.employee_info[0]._id : '',
                            nik: x.employee_info.length ? x.employee_info[0].employee_code : '',
                            departement_info: x.departement_info.length ? x.departement_info[0].name : '',
                            total_salary: (x.basic_salary + x.positional_allowance + x.fixed_allowance)
                        });
                    })

                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    let result = await this.getModel(id, req.cookies['company_selected_transaction']);
                    let list = {
                        _id: result._id,
                        employee_id: ObjectId(result.employee_id),
                        basic_salary: result.basic_salary,
                        positional_allowance: result.positional_allowance,
                        fixed_allowance: result.fixed_allowance,
                        detail: result.allowance_deduction_distribution_info
                    };

                    this.jsonResponse.setData(list);
                } catch(e) {
                    console.log(e)
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            store: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new SalaryModel;
                    
                    req.body = qs.parse(req.body);
                    // Get Id
                    let id = req.body._id;
                    
                    if(id) {
                        model = await SalaryModel.findOne({_id: id}).exec();
                    }
                    
                    let repo = new Salary(model);
                    
                    model.employee_id = ObjectId(req.body.employee_id);
                    model.basic_salary = parseFloat(req.body.basic_salary);
                    model.positional_allowance = parseFloat(req.body.positional_allowance);
                    model.fixed_allowance = parseFloat(req.body.fixed_allowance);
                    
                    model.created_at = !id ? now : undefined;
                    model.created_by = !id ? ObjectId(req.user.user_id) : undefined;
                    model.updated_at = id ? now : undefined;
                    model.updated_by = id ? ObjectId(req.user.user_id) : undefined;

                    // Delete detail
                    req.body._delete_detail = req.body._delete_detail || [];
                    if(req.body._delete_detail) {
                        req.body._delete_detail.forEach(async x => {
                            await repo.deleteDetail(x);
                        });
                    }

                    // Save detail Deskription
                    req.body.detail = req.body.detail || [];
                    if(req.body.detail.length) {
                        req.body.detail.forEach(x => {
                            let id = !x._id ? null : x._id;
                            repo.addDetail(id, req.body.employee_id, x.salary_description_id, x.name, x.code == '+' ? true : false, x.amount, now)
                        })
                    }
                    
                    let result = await repo.save();

                    this.jsonResponse.setMessage(`${result['data'].fullname}  telah berhasil tersimpan.`);
                    this.jsonResponse.setData(result['data']._id);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            storeMultipleEmployee: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new SalaryModel;
                    
                    req.body = qs.parse(req.body);

                    let repo = new Salary(model);

                    req.body.detail = req.body.detail || [];
                    console.log(req.body.employee)
                    req.body.employee = req.body.employee || [];
                    
                    if(!req.body.employee.length)
                        throw new Error('Data karyawan tidak ada')

                    req.body.employee.forEach(x => {
                        if(req.body.detail.length) {
                            req.body.detail.forEach(b => {
                                let id = !b._id ? null : b._id;
                                repo.addDetail(id, x, b.salary_description_id, b.name, b.code == '+' ? true : false, b.amount, now)
                            })
                        }
                    });
                        
                    await repo.saveMultiple();

                    this.jsonResponse.setMessage(`Telah berhasil tersimpan.`);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            destroy: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;
                    
                    let row = await this.getModel(id, req.cookies['company_selected_transaction']);
                    let repo = new Salary(row);
                    await repo.deleteOne({_id: id});
                    
                    let message = `${row.name} berhasil dihapus`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            auto_generate: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    
                    let gen = 'u';
                    if(req.query.key == 'username') {
                        gen = username(req.query.string);
                    } else {
                        gen = password(req.query.string);
                    }
                    

                    let message = `Success`;
                    this.jsonResponse.setData({gen: gen})
                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            download: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let row = await this.getModel(req.params.id, req.cookies['company_selected_transaction']);
                   
                    let repo = new Salary(row);

                    let data = row;
                    
                    let cookiesSelected = req.cookies['company_selected_transaction'];
                    let filename = await repo.download(data, cookiesSelected);

                    let returnData = {
                        _id: req.body.id,
                        url: '/tmp/' + filename
                    };

                    let message = `Success`;
                    this.jsonResponse.setData(returnData);
                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let employee = new SalaryController();
module.exports.SalaryController = employee.controller();