const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');
const qs = require('qs');

const PaidLeave = require("../../Ais/Repository/PaidLeave");
const Setting = require("../../Ais/Repository/Setting")

const PaidLeaveModel = require("../../models/PaidLeaveModel").PaidLeave;
const Status = require("../../models/CategoryModel").Categories;
const Category = require("../../models/CategoryModel").Category;
const Employee = require("../../models/EmployeeModel").Employee;

const PaidLeaveFinder = require("../../Ais/Repository/Finder/PaidLeaveFinder");

class PaidLeaveController extends ApiController
{
    async getModel(id)
    {
        let row = await PaidLeaveModel.findOne({_id: id}).populate({
            path: 'status_info',
            model: Category
        })
        .populate({
            path: 'employee_info',
            model: Employee,
            populate: [{
                path: 'employee_type_info'
            }]
        })
        .exec();

        if(!row)
            throw new Error('PaidLeave tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let repoSetting = new Setting;
                let finder = new PaidLeaveFinder(req.query);
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {
                        finder.orderBy('created_at', -1);
                    }

                    if(req.query.status) {
                        let statusId = Status.getStatusIdByName(req.query.status);
                        finder.setStatusId(statusId);
                    }
                    
                    if(req.user_info.role_type_code.code == 6)
                        finder.setByEmployee(req.user.user_id)

                    finder.setByYearTransaction(req.cookies['company_selected_transaction'])
                    
                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        list.push({
                            fullname: x.employee_info.length ? x.employee_info[0].fullname : '',
                            status_info: x.status_info.length ? x.status_info[0].label : '',
                            employee_status_info: x.employee_status_info.length ? x.employee_status_info[0].label : '',
                            email: x.employee_info.length ? x.employee_info[0].email : '',
                            join_date: x.employee_info.length ? x.employee_info[0].join_date : '',
                            photo: x.employee_info.length ? x.employee_info[0].photo : '',
                            created_at: x.created_at,
                            updated_at: x.updated_at,
                            _id: x._id,
                            created_by: x.created_by,
                            employee_id: x.employee_info.length ? x.employee_info[0].employee_id : '',
                            departement_info: x.departement_info.length ? x.departement_info[0].name : '',
                            start_date: x.start_date,
                            end_date: x.end_date,
                            status: x.status_paid_leave,
                            category_paid_leave_info: x.category_paid_leave_info,
                            date_of_filing: x.date_of_filing
                        });
                    })

                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    
                    let result = await this.getModel(id);
                    let list = {
                        _id: result._id,
                        category_leave_id: String(result.category_leave_id),
                        start_date: result.start_date,
                        end_date: result.end_date,
                        notes: result.notes,
                        status: result.status_info,
                        status_id: String(result.status_id),
                        created_at: result.created_at,
                        created_by: result.created_by,
                        updated_at: result.updated_at,
                        updated_by: result.updated_by
                    };

                    this.jsonResponse.setData(list);

                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse())
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                    
                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse());
                }
            },
            store: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                
                try {
                    if(!req.body.status)
                    req.body.status = null;

                    let model = new PaidLeaveModel;

                    // Get Id
                    let id = req.body._id;
                    
                    if(id) {
                        model = await PaidLeaveModel.findOne({_id: id}).exec();
                    }

                    let repoPaidLeave = new PaidLeave(model);
                    
                    model.start_date = req.body.start_date;
                    model.end_date = req.body.end_date;
                    model.employee_id = req.user.user_id;
                    model.category_leave_id = req.body.category_leave_id;
                    model.notes = req.body.notes;
                    
                    model.created_at = !id ? now : model.created_at;
                    model.created_by = !id ? ObjectId(req.user.user_id) : model.created_by;
                    model.updated_at = id ? now : undefined;
                    model.updated_by = id ? ObjectId(req.user.user_id) : undefined;
                    
                    // Save
                    let result = await repoPaidLeave.save();
                    
                    // After save change status
                    switch (req.body.status) {
                        case 'pending':
                            model['isNew'] = false;
                            model.date_of_filing = now;
                            await repoPaidLeave.setStatus(Status.STATUS_PENDING);
                            break;
                    }

                    this.jsonResponse.setMessage(`Telah berhasil tersimpan.`);
                    this.jsonResponse.setData(result['data']._id);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            destroy: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;
                    
                    let row = await this.getModel(id);
                    let repo = new PaidLeave(row);
                    await repo.deleteOne({_id: id});
                    
                    let message = `${row.name} berhasil dihapus`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            download: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let row = await this.getModel(req.query.id);
                   
                    let repo = new PaidLeave(row);

                    let data = row;
                    
                    let filename = await repo.download(data);

                    let returnData = {
                        _id: req.body.id,
                        url: '/tmp/' + filename
                    };

                    let message = `Success`;
                    this.jsonResponse.setData(returnData);
                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let employee = new PaidLeaveController();
module.exports.PaidLeaveController = employee.controller();