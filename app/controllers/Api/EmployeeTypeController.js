const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const photo = require("../../Ais/Helpers/ResourceUrl");
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');

const Model = require("../../models/CategoryModel").Category;
const UserModel = require("../../models/UserModel").User;

const EmployeeTypeFinder = require("../../Ais/Repository/Finder/EmployeeTypeFinder");

class EmployeeTypeController extends ApiController
{
    async getModel(id)
    {
        let row = await Model.findOne({_id: id}).exec();

        if(!row)
            throw new Error('Employee Type tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new EmployeeTypeFinder(req.query);
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {
                        finder.orderBy('created_at', -1);
                    }

                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        list.push({
                            _id: x._id,
                            name: x.name,
                            label: x.label,
                            notes: x.notes,
                            remaining_days_off: x.remaining_days_off_info.length ? x.remaining_days_off_info[0].count : 0
                        });
                    })

                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    
                    let result = await this.getModel(id);
                    
                    let list = {
                        _id: result._id,
                        
                        created_at: result.created_at,
                        created_by: result.created_by,
                        updated_at: result.updated_at,
                        updated_by: result.updated_by
                    };

                    this.jsonResponse.setData(list);

                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse())
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                    
                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse());
                }
            },
            store: async (req,res) => 
            {
                
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new Model;
                    let modelUser = new UserModel;

                    // Get Id
                    let id = req.body._id;
                    
                    if(id) {
                        model = await Model.findOne({_id: id}).exec();
                    }

                    

                    this.jsonResponse.setMessage(`${result['data'].fullname}  telah berhasil tersimpan.`);
                    this.jsonResponse.setData(result['data']._id);

                } catch(e) {
                    console.log(e)
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            destroy: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;
                    
                    let row = await this.getModel(id);
                    let repo = new Employee(row);
                    await repo.deleteOne({_id: id});
                    
                    let message = `${row.name} berhasil dihapus`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            auto_generate: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    
                    let gen = 'u';
                    if(req.query.key == 'username') {
                        gen = username(req.query.string);
                    } else {
                        gen = password(req.query.string);
                    }
                    

                    let message = `Success`;
                    this.jsonResponse.setData({gen: gen})
                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let employeeType = new EmployeeTypeController();
module.exports.EmployeeTypeController = employeeType.controller();