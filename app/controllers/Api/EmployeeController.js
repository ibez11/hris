const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const photo = require("../../Ais/Helpers/ResourceUrl");
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');
const { username } = require("../../TheBadusLibs/Generate");
const { password } = require("../../TheBadusLibs/Generate");
const PasswordGenerate = require("../../TheBadusLibs/ThePasswordGenerate");
const qs = require('qs');

const Employee = require("../../Ais/Repository/Employee");

const EmployeeModel = require("../../models/EmployeeModel").Employee;
const UserModel = require("../../models/UserModel").User;

const EmployeeFinder = require("../../Ais/Repository/Finder/EmployeeFinder");

class EmployeeController extends ApiController
{
    async getModel(id)
    {
        let row = await EmployeeModel.findOne({_id: id}).populate('user_info').exec();

        if(!row)
            throw new Error('Employee tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new EmployeeFinder(req.query);
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {
                        finder.orderBy('created_at', -1);
                    }

                    if(req.query.head) {
                        finder.setByHead(req.query.head.employee_type_id);
                        finder.setByDepartement(req.query.head.departement_id);
                    }
                        
                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        list.push({
                            fullname: x.fullname,
                            dob: x.dob,
                            gender: x.gender,
                            status_info: x.status_info.length ? x.status_info[0].label : '',
                            father_name: x.father_name,
                            nationality_info: x.nationality_info.length ? x.nationality_info[0].name : '',
                            pasport_number: x.pasport_number,
                            address: x.address,
                            city: x.city,
                            country: x.country_info.length ? x.country_info.name : '',
                            hp: x.hp,
                            phone_number: x.phone_number,
                            employee_status_info: x.employee_status_info.length ? x.employee_status_info[0].label : '',
                            email: x.email,
                            join_date: x.join_date,
                            photo: x.photo,
                            created_at: x.created_at,
                            updated_at: x.updated_at,
                            _id: x._id,
                            created_by: x.created_by,
                            employee_code: x.employee_code,
                            departement_info: x.departement_info.length ? x.departement_info[0].name : '',
                            username: x.user_info.length ? x.user_info[0].username : '',
                            password: x.user_info.length ? x.user_info[0].password : ''
                        });
                    })
                    
                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    
                    let result = await this.getModel(id);
                    
                    let list = {
                        _id: result._id,
                        fullname: result.fullname,
                        dob: result.dob,
                        gender: result.gender,
                        status_id: String(result.status_id),
                        father_name: result.father_name,
                        nationality_id: String(result.nationality_id),
                        paspor_number: result.paspor_number,
                        address: result.address,
                        city: result.city,
                        country_id: String(result.country_id),
                        hp: result.hp,
                        phone_number: result.phone_number,
                        email: result.email,
                        employee_code: result.employee_code,
                        parent_id: result.parent_id,
                        departement_id: result.departement_id,
                        employee_status_id: String(result.employee_status_id),
                        employee_type_id: String(result.employee_type_id),
                        join_date: result.join_date,
                        photo: result.photo,
                        photo_url: photo(result.photo, 'img/foto_karyawan'),
                        username: result.user_info ? result.user_info.username : '',
                        password: result.user_info ? result.user_info.password : '',
                        created_at: result.created_at,
                        created_by: result.created_by,
                        updated_at: result.updated_at,
                        updated_by: result.updated_by
                    };

                    this.jsonResponse.setData(list);

                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse())
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                    
                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse());
                }
            },
            store: async (req,res) => 
            {
                
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new EmployeeModel;
                    let modelUser = new UserModel;

                    // Get Id
                    let id = req.body._id;
                    
                    if(id) {
                        model = await EmployeeModel.findOne({_id: id}).exec();
                    }

                    let repoEmployee = new Employee(model);
                    
                    model.fullname = req.body.fullname;
                    model.dob = req.body.dob;
                    model.gender = req.body.gender;
                    model.status_id = req.body.status_id;
                    model.father_name = req.body.father_name;
                    model.nationality_id = req.body.nationality_id;
                    model.paspor_number = req.body.paspor_number;
                    model.address = req.body.address;
                    model.city = req.body.city;
                    model.parent_id = req.body.parent_id;
                    model.country_id = req.body.country_id;
                    model.hp = req.body.hp;
                    model.remaining_days_off = req.body.remaining_days_off;
                    model.phone_number = req.body.phone_number;
                    model.email = req.body.email;
                    model.employee_code = req.body.employee_code;
                    model.departement_id = req.body.departement_id;
                    model.employee_status_id = req.body.employee_status_id;
                    model.employee_type_id = req.body.employee_type_id;
                    model.join_date = req.body.join_date;
                    model.photo = req.body.photo;

                    model.created_at = !id ? now : undefined;
                    model.created_by = !id ? ObjectId(req.user.user_id) : undefined;
                    model.updated_at = id ? now : undefined;
                    model.updated_by = id ? ObjectId(req.user.user_id) : undefined;
                    
                    repoEmployee.addUser(modelUser);
                    
                    modelUser.username = req.body.username;
                    modelUser.password = req.body.password == req.body.current_password ? req.body.current_password : PasswordGenerate(req.body.password);
                    modelUser.is_actived = true;

                    let result = await repoEmployee.save();

                    this.jsonResponse.setMessage(`${result['data'].fullname} telah berhasil tersimpan.`);
                    this.jsonResponse.setData(result['data']._id);

                } catch(e) {
                    console.log(e)
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            destroy: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;
                    
                    let row = await this.getModel(id);
                    let repo = new Employee(row);
                    await repo.deleteOne({_id: id});
                    
                    let message = `${row.name} berhasil dihapus`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            auto_generate: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    
                    let gen = 'u';
                    if(req.query.key == 'username') {
                        gen = username(req.query.string);
                    } else {
                        gen = password(req.query.string);
                    }
                    

                    let message = `Success`;
                    this.jsonResponse.setData({gen: gen})
                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let employee = new EmployeeController();
module.exports.EmployeeController = employee.controller();