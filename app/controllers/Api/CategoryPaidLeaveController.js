const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const photo = require("../../Ais/Helpers/ResourceUrl");
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');
const { username } = require("../../TheBadusLibs/Generate");
const { password } = require("../../TheBadusLibs/Generate");
const PasswordGenerate = require("../../TheBadusLibs/ThePasswordGenerate");
const qs = require('qs');

const CategoryPaidLeave = require("../../Ais/Repository/CategoryPaidLeave");

const Model = require("../../models/CategoryModel").Category;
const UserModel = require("../../models/UserModel").User;

const CategoryPaidLeaveFinder = require("../../Ais/Repository/Finder/CategoryPaidLeaveFinder");

class CategoryPaidLeaveController extends ApiController
{
    async getModel(id)
    {
        let row = await Model.findOne({_id: id}).populate('user_info').exec();

        if(!row)
            throw new Error('Employee tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new CategoryPaidLeaveFinder(req.query);
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {
                        finder.orderBy('created_at', -1);
                    }
                    
                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        list.push({
                            _id: x._id,
                            name: x.name,
                            label: x.label,
                            notes: x.notes
                        });
                    })

                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    
                    let result = await this.getModel(id);
                    
                    let list = {
                        _id: result._id,
                        name: result.name,
                        label: result.label,
                        notes: result.notes
                    };

                    this.jsonResponse.setData(list);

                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse())
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                    
                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse());
                }
            },
            store: async (req,res) => 
            {
                
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new Model;

                    // Get Id
                    let id = req.body._id;
                    
                    if(id) {
                        model = await Model.findOne({_id: id}).exec();
                    }

                    let repo = new CategoryPaidLeave(model);
                    
                    model.name = req.body.name;
                    model.label = req.body.label;
                    model.notes = req.body.notes;
                    model.group_by = 'reason_leave';

                    model.created_at = !id ? now : undefined;
                    model.updated_at = id ? now : undefined;

                    let result = await repo.save();

                    this.jsonResponse.setMessage(`${result['data'].label}  telah berhasil tersimpan.`);
                    this.jsonResponse.setData(result['data']._id);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            destroy: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;
                    
                    let row = await this.getModel(id);
                    let repo = new CategoryPaidLeave(row);
                    await repo.deleteOne({_id: id});
                    
                    let message = `${row.label} berhasil dihapus`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            auto_generate: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    
                    let gen = 'u';
                    if(req.query.key == 'username') {
                        gen = username(req.query.string);
                    } else {
                        gen = password(req.query.string);
                    }
                    

                    let message = `Success`;
                    this.jsonResponse.setData({gen: gen})
                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let paidleave = new CategoryPaidLeaveController();
module.exports.CategoryPaidLeaveController = paidleave.controller();