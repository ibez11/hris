const ApiController = require('./ApiController');
const {ObjectId} = require('mongodb'); // or ObjectID
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');
const qs = require('qs');

const ModelReq = require("../../models/SysCtrlModel");

const Setting = require("../../Ais/Repository/Setting");

class SettingController extends ApiController
{
    controller()
    {
        return {
            index: async (req,res) => 
            {
                this.jsonResponse = this.jResp();

                try {
                    const Model = ModelReq.SysCtrl({_id: String, value: {type: String}});
            
                    var setting = await Model.find({}).lean();
                    
                    let settingList = [];
                    setting.forEach(x => {
                        settingList.push({
                            key: x._id,
                            value: x.value
                        });
                    });
                    
                    let list = {};
                    await Promise.all(
                        settingList.map(function(el) {
                            list[el['key']] = el['value'];
                        })
                    )

                    this.jsonResponse.setData(list);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }
                
                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();

                try {
                    let key = req.params.id;
                    const Model = ModelReq.SysCtrl({_id: String, value: {type: String}});
            
                    var row = await Model.findOne({_id: key}).lean();
                    
                    this.jsonResponse.setData(row);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }
                
                return res.status(200).send(this.jsonResponse.getResponse());
            },
            store: async (req,res) => 
            {
                let repo = new Setting();
                
                try {
                    let data = JSON.parse(req.body.data);

                    Object.keys(data).forEach(x => {
                        if(x != 'api_token')
                        repo.update(x, data[x]);
                    })

                    let result = await repo.save();
                    
                    this.jsonResponse.setData(result);
                    this.jsonResponse.setMessage('Data telah berhasil tersimpan.');
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }
                
                return res.status(200).send(this.jsonResponse.getResponse());
            }
        }
    }
}

let setting = new SettingController();
module.exports.SettingController = setting.controller();