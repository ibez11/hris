const AccessControl = require("../../Ais/Repository/AccessControl");
const JsonResponse = require("../../TheBadusLibs/JsonResponse");
const UploadImage = require("../../TheBadusLibs/Upload");

class ApiController
{
    constructor() {
        this.jsonResponse = this.jResp();
    }

    jResp()
    {
        return new JsonResponse();
    }

    upload(image)
    {
        if(image)
            return new UploadImage(image);

        return null;
    }

    getAccessControl(id)
    {
        if(id)
            return new AccessControl(id);

        return null;
    }
}


module.exports = ApiController;