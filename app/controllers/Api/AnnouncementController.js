const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');
const qs = require('qs');

const Announcement = require("../../Ais/Repository/Announcement");

const AnnouncementModel = require("../../models/AnnouncementModel").Announcement;

const AnnouncementFinder = require("../../Ais/Repository/Finder/AnnouncementFinder");

class AnnouncementController extends ApiController
{
    async getModel(id)
    {
        let row = await AnnouncementModel.findOne({_id: id}).exec();

        if(!row)
            throw new Error('Announcement tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new AnnouncementFinder(req.query);
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {    
                        finder.orderBy('created_at', 'desc');
                    }
                    
                    var paginator = await finder.get();
                    
                    this.jsonResponse.setData(paginator.data);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    
                    let result = await this.getModel(id);
                    
                    this.jsonResponse.setData(result);

                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse())
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                    
                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse());
                }
            },
            store: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    req = qs.parse(req);
                    
                    let model = new AnnouncementModel;
        
                    // Get Id
                    let id = req.body._id;
                    
                    if(id){
                        model = await AnnouncementModel.findOne({_id: id}).exec();
                    }

                    let repoAnnouncement = new Announcement(model);
                    
                    model.title = req.body.title;
                    model.description = req.body.description;
                    model.short_description = req.body.short_description;
                    
                    model.created_at = !id ? now : undefined;
                    model.created_by = !id ? ObjectId(req.user.user_id) : undefined;
                    model.updated_at = id ? now : undefined;
                    model.updated_by = id ? ObjectId(req.user.user_id) : undefined;
                    
                    let result = await repoAnnouncement.save();
                    
                    this.jsonResponse.setMessage(`${result['data'].title}  telah berhasil tersimpan.`);
                    this.jsonResponse.setData(result['data']._id);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            destroy: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;
                    
                    let row = await this.getModel(id);
                    let repo = new Announcement(row);
                    await repo.deleteOne({_id: id});
                    
                    let message = `${row.title} berhasil dihapus`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            publish: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    req.body = qs.parse(req.body);
                    
                    let model = new AnnouncementModel;
        
                    let repoAnnouncement = new Announcement(model);
                    
                    req.body.details.forEach(x => {
                        repoAnnouncement.addDetail(ObjectId(x._id), now, req.user.user_id);
                    })

                    let result = await repoAnnouncement.publish();
                    
                    this.jsonResponse.setMessage(result.message);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let departement = new AnnouncementController();
module.exports.AnnouncementController = departement.controller();