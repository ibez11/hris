var GroupingDetail = function(details) {
    this.details = details;

    // Grouping quote details
    this._groupingDetail = function(details) {
        var list = [];
        var i, x;
        var group;
        var total = 0;

        for(i=0; i<details.length; i++) {
            x = details[i];
            x.amount = parseFloat(x.amount);

            group = list.find(function(el) {
                return el.name === x.group_by;
            });

            if(group){
                total += x.amount;
                group.detail.push(x);
                group.total = total;
            }else{
                total = x.amount;
                list.push({
                    name: x.group_by,
                    total : total,
                    detail: [x]
                });
            }
        }
        return list;
    };

    this.get = function() {
        return this._groupingDetail(this.details);
    };
};
