var q = function() {
  this.date = function(date) {
      return moment(date).format('YYYY-MM-DD');
  };
  this.dateCustomize = function(date, format) {
    return moment(date).format(format);
  };
};
var DateFormatter = new q();