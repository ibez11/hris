(function() {

'use strict';

angular.module('app.service').service('ApiService', ApiService);

function ApiService(
  $http, $httpParamSerializer, ApiTokenService) {
  var apiToken = ApiTokenService.get();

  this.Dashboard = {};

  this.Country = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/country/list?' + $.param(params);
      
      return $http.get(url);
    }
  };

  this.GenerateAuto = {
    gen: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/employee/generate?' + $.param(params);
      
      return $http.get(url);
    }
  };

  this.Setting = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/setting?' + $.param(params);
      
      return $http.get(url);
    },
    get:  function(id) {
      var url = '/api/setting/' + id +'?api_token=' + apiToken;
      
      return $http.get(url);
    },
    setSelectedTransaction:  function(post) {
      var params = post;
      params.api_token = apiToken;
      var url = '/api/setting';
      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
  };

  this.SalaryDescription = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/salary-description/list?' + $.param(params);
      
      return $http.get(url);
    },
    get:  function(id) {
      var url = '/api/master/salary-description/detail/' + id +'?api_token=' + apiToken;
      
      return $http.get(url);
    },
  };

  this.Statuses = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/statuses/list?' + $.param(params);
      
      return $http.get(url);
    }
  };

  this.EmployeeStatuses = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/employee-statuses/list?' + $.param(params);
      
      return $http.get(url);
    }
  };

  this.EmployeeType = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/employee-type/list?' + $.param(params);
      
      return $http.get(url);
    }
  };

  this.Departement = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/departement/list?' + $.param(params);
      
      return $http.get(url);
    },
    get:  function(id) {
      var url = '/api/master/departement/detail/' + id +'?api_token=' + apiToken;
      
      return $http.get(url);
    },
    delete: function(id) {
      var url = '/api/master/departement/delete/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    },
    create: function(post) {
      var params = post;
      params.api_token = apiToken;
      var url = '/api/master/departement/create';
      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    }
  };

  this.CurrentFinancialMonthYear = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/currentfinancialmonthyear/list?' + $.param(params);
      
      return $http.get(url);
    },
    get:  function(id) {
      var url = '/api/master/currentfinancialmonthyear/detail/' + id +'?api_token=' + apiToken;
      
      return $http.get(url);
    },
    setSelected: function(post) {
      var params = post;
      params.api_token = apiToken;
      var url = '/api/master/currentfinancialmonthyear/setselected';
      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    monthEnd: function() {
      var params = {};
      params.api_token = apiToken;
      var url = '/api/master/currentfinancialmonthyear/month-end';
      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    }
  };

  this.Employee = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/employee/list?' + $.param(params);
      
      return $http.get(url);
    },
    get:  function(id) {
      var url = '/api/master/employee/detail/' + id +'?api_token=' + apiToken;
      
      return $http.get(url);
    },
    create: function(post) {
      var params = post;
      params.api_token = apiToken;
      var url = '/api/master/employee/create';
      
      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    }
  };

  this.Salary = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/salary/list?' + $.param(params);
      
      return $http.get(url);
    },
    get:  function(id) {
      var url = '/api/master/salary/detail/' + id +'?api_token=' + apiToken;
      
      return $http.get(url);
    },
    delete: function(id) {
      var url = '/api/master/salary/delete/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    },
    create: function(post) {
      var params = post;
      params.api_token = apiToken;
      var url = '/api/master/salary/create';
      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    createMultipleEmployee: function(post) {
      var params = post;
      params.api_token = apiToken;
      var url = '/api/master/salary/create_multiple';
      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    download: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/salary/download/' + params.id + '?' + $.param(params);

      return $http.get(url);
    },
  };

  this.Announcement = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/announcement/list?' + $.param(params);
      
      return $http.get(url);
    },
    get:  function(id) {
      var url = '/api/master/announcement/detail/' + id +'?api_token=' + apiToken;
      
      return $http.get(url);
    },
    delete: function(id) {
      var url = '/api/master/announcement/delete/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    },
    create: function(post) {
      var params = post;
      params.api_token = apiToken;
      var url = '/api/master/announcement/create';
      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    publish: function(post) {
      var params = post;
      params.api_token = apiToken;
      var url = '/api/master/announcement/publish';
      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
  };

  this.PaidLeave = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/paid-leave/list?' + $.param(params);
      
      return $http.get(url);
    },
    get:  function(id) {
      var url = '/api/master/paid-leave/detail/' + id +'?api_token=' + apiToken;
      
      return $http.get(url);
    },
    delete: function(id) {
      var url = '/api/master/paid-leave/delete/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    },
    create: function(post) {
      var params = post;
      params.api_token = apiToken;
      var url = '/api/master/paid-leave/create';
      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    download: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/paid-leave/download/' + params.id + '?' + $.param(params);

      return $http.get(url);
    },
  };

  this.CategoryPaidLeave = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/category-paid-leave/list?' + $.param(params);
      
      return $http.get(url);
    },
    get:  function(id) {
      var url = '/api/master/category-paid-leave/detail/' + id +'?api_token=' + apiToken;
      
      return $http.get(url);
    },
    delete: function(id) {
      var url = '/api/master/category-paid-leave/delete/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    },
    create: function(post) {
      var params = post;
      params.api_token = apiToken;
      var url = '/api/master/category-paid-leave/create';
      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    }
  };

  this.PaidLeaveRequest = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/master/paid-leave-request/list?' + $.param(params);
      
      return $http.get(url);
    },
    get:  function(id) {
      var url = '/api/master/paid-leave-request/detail/' + id +'?api_token=' + apiToken;
      
      return $http.get(url);
    },
    setStatus:  function(id, status) {
      var url = '/api/master/paid-leave-request/' + id +'/set-status/' + status +'?api_token=' + apiToken;
      
      return $http.get(url);
    }
  };

  this.Setting = {
    get: function() {
      var url = '/api/setting?api_token=' + apiToken;

      return $http.get(url);
    },
    create: function(params) {
      var newParams = {
          api_token: apiToken,
          data: JSON.stringify(params)
      }
      // params.api_token = apiToken;
      var url = '/api/setting';

      return $http({
      method: 'POST',
      url:url,
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: $.param(newParams)
      });
    },
  }

};

})();
