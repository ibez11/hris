(function() {
    'use strict';

    angular.module('app.service').service('SelectedMonthYearTransactionService', SelectedMonthYearTransactionService);

    function SelectedMonthYearTransactionService($cookies) {
      // 1 day = 24(hours) * 60(minutes) * 60(seconds)
      this.expired = 24 * 60 * 60;

      this.get = function() {
        return $cookies.get('company_selected_transaction');
      }

      this.set = function(value) {
        var expired = new Date().getTime() + this.expired * 1000;
        
        $cookies.put('company_selected_transaction', value, {'expires' : new Date(expired)});
      }
    }
})();