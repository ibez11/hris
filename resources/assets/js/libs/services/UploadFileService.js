(function() {
    'use strict';

    angular.module('app.service').service('UploadFileService', UploadFileService);

    function UploadFileService(Upload, ApiTokenService) {
        var apiToken = ApiTokenService.get();
        
        this.onFileSelect = function(file) {
            if(file)
            file.upload = Upload.upload({
                url: '/api/upload/image?api_token='+apiToken, //upload.php script, node.js route, or servlet url
                data: {filename: file.$ngfName},
                file: file,
            }).progress(function(evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            }).then(function(response) {
                var data = response.data;
                file.result = data.data.url;
                file.filename = data.data.filename;
            });
        };
    }
})();