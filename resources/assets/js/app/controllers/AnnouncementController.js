(function() {
    'use strict';
    
    angular.module('app').controller('AnnouncementController', AnnouncementController);
    
    function AnnouncementController($state, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
    
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
    
        // Paginator
        this.paginator = {};
        
        this.fm = FlashMessageService;
        this.ls = LoadingService;
        
        this.published = [];
        // Warranty card
        this.list = null;
        this.selectedHash = null;
        this.propertyName = 'label';
        this.reverse = true;
        this.prop = 0;
    
        this.createEmptyData = function() {
            return {
                _id: null,
                title: null,
                short_description: null,
                description: null,
                _hash: Math.getRandomHash(),
            };
        };

        this.setTitle = function() {
            this.title = "List Pengumuman";
        };
    
        this.init = function() {
            this.createEmptyData();
            this.setTitle();
            
            this.selectedHash = null;
            var search = {};

            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.is_actived)
                search.is_actived = $stateParams.is_actived;
            if($stateParams.approved)
                search.approved = $stateParams.approved;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
    
            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }
    
            vm.search = search;
            vm._doSearch();
        };
    
        // Search invoice
        this._doSearch = function() {
            vm.ls.get('loading').on();
            
            ApiService.Announcement.all(vm.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;
                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }
    
                // Paginator
                vm.paginator = Paginator(data);
    
                vm.ls.get('loading').off();
            });
        };
    
        this.create = function() {
            vm.ls.get('loading').on();
            this.createEmptyData();
            var id = $stateParams.id;
            if(id){
                ApiService.Announcement.get(id).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;

                        vm.data = data;
                    } else {
                        vm.error.on();
                        vm.message = resp.data.errors;
                        $state.go('announcement', this.search);
                    }
                });
            }

            vm.ls.get('loading').off();
        };
    
        // Submit to DB
        this.submit = function(){
            vm.ls.get('loading').on();
            var postData = this._createPostData(this.data);
            
            ApiService.Announcement.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    vm.init();
    
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
    
                vm.ls.get('loading').off();
            });
    
        };
        
        this.add = function(x) {
            var addParam = {
                _id: x._id,
                published: x.is_published,
                _hash: Math.getRandomHash()
            };

            this.published.push(addParam);
        };

        this.submitPublished = function() {
            vm.ls.get('loading').on();
            var postData = this._createPublishPostData(this.published);
            ApiService.Announcement.publish(postData).then(function(resp){
                if(!resp.data.is_error) {
                    vm.init();
    
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
    
                vm.ls.get('loading').off();
            });
        }

        this._createPublishPostData = function(data){
            var postData = {};
            postData.details = JSON.parse(JSON.stringify(data))
            
            return postData;
        };
    
        this._createPostData = function(data){
            var postData = {
                _id: data._id,
                title: data.title,
                short_description: data.short_description,
                description: data.description
            };
    
            return postData;
        };
    
        this.delete = function(id) {
            vm.ls.get('loading').on();
            ApiService.Announcement.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm._doSearch();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
                vm.ls.get('loading').off();
            });
        };

        // Redirect to correct route
        this.doSearch = function() {
            this.search.order_by = this.orderBy.toString();
            $state.go('announcement', this.search);
        };
        
        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
            
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    };
})();