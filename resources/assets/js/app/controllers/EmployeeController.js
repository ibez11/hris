(function() {
    'use strict';
    
    angular.module('app').controller('EmployeeController', EmployeeController);
    
    function EmployeeController($state, $stateParams, ApiService, UploadFileService, LoadingService,
    FlashMessageService) {
        var vm = this;
        
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
    
        // Paginator
        this.paginator = {};
        
        this.uf = UploadFileService;
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this.selectedHash = null;
        this.propertyName = 'label';
        this.reverse = true;
        this.prop = 0;
    
        this.createEmptyData = function() {
            return {
                fullname: null,
                dob: null,
                gender: null,
                status_id: null,
                father_name: null,
                nationality_id: null,
                pasport_number: null,
                address: null,
                city: null,
                country_id: null,
                remaining_days_off: null,
                hp: null,
                phone_number: null,
                email: null,
                parent_id: null,
                employee_code: null,
                departement_id: null,
                employee_status_id: null,
                employee_type_id: null,
                join_date: null,
                photo: null,
                username: null,
                password: null,
                _hash: Math.getRandomHash(),
            };
        };

        this.setTitle = function() {
            this.title = "List Karyawan";
        };
    
        this.init = function() {
            this.createEmptyData();
            this.setTitle();
            
            this.selectedHash = null;
            var search = {};

            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.is_actived)
                search.is_actived = $stateParams.is_actived;
            if($stateParams.approved)
                search.approved = $stateParams.approved;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
            
            // Order By
            if($stateParams.order_by) {
                
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }
    
            vm.search = search;
            vm._doSearch();
        };

        this.employeeStatuses = function(){
            ApiService.EmployeeStatuses.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.employee_statuses = data.data;

                vm.employee_statuses._id = String(vm.employee_statuses._id);
                vm.ls.get('loading-employee-statuses').off();
            });
        };

        this.departement = function(){
            ApiService.Departement.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.departements = data.data;
                
                vm.departements._id = String(vm.departements._id);
                vm.ls.get('loading').off();
            });
        };

        this.status = function(){
            ApiService.Statuses.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.statuses = data.data;
                
                vm.statuses._id = String(vm.statuses._id);
                vm.ls.get('loading-status').off();
            });
        }

        this.genders = function(){
            var genders = [
                {id: "1", name: "Pria"},
                {id: "2", name: "Wanita"}
            ];
            
            vm.genders = genders;
            vm.genders.id = String(vm.genders.id);
            vm.ls.get('loading-genders').off();
        }
    
        // Search invoice
        this._doSearch = function() {
            vm.ls.get('loading').on();
            
            ApiService.Employee.all(vm.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;
                
                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }
    
                // Paginator
                vm.paginator = Paginator(data);
    
                vm.ls.get('loading').off();
            });
        };

        // Add blank category
        this.add = function() {
            vm.createEmptyData();
            vm.prop = 2;
        };
    
        this.create = function() {
            vm.ls.get('loading').on();
            vm.ls.get('loading-country').on();
            vm.ls.get('loading-genders').on();
            vm.ls.get('loading-status').on();
            vm.ls.get('loading-departement').on();
            vm.ls.get('loading-employee-statuses').on();
            vm.ls.get('loading-employee-types').on();

            this.createEmptyData();
            
            var id = $stateParams.id;
            
            if(id){
                ApiService.Employee.get(id).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;
                        vm.data = data;

                        var employeeTypeId = vm.data.employee_type_id;
                        var employeeDepartementId = vm.data.departement_id;
                        ApiService.Employee.all({head: {employee_type_id: employeeTypeId, departement_id: employeeDepartementId}}).then(function(resp) {
                            var data = resp.data;
                            vm.employees = data.data;

                            vm.employees._id = String(vm.employees._id);
                        });
                    } else {
                        vm.error.on();
                        vm.message = resp.data.errors;
                    }
                    vm.ls.get('loading').off();
                });
            }
            
            this.status();
            this.departement();
            this.employeeStatuses();

            ApiService.Country.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.countries = data.data;

                vm.countries._id = String(vm.countries._id);
                vm.ls.get('loading-country').off();
            });

            ApiService.EmployeeType.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.employee_types = data.data;

                vm.employee_types._id = String(vm.employee_types._id);
                vm.ls.get('loading-employee-types').off();
            });

            this.genders();
        };
    
        // Submit to DB
        this.submit = function(){
            vm.ls.get('loading').on();
            
            var postData = this._createPostData(this.data);
            console.log(postData)
            ApiService.Employee.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    vm.init();
    
                    vm.fm.success(resp.data.message);
                    $state.go('employee');
                } else {
                    vm.fm.error(resp.data.message);
                }
    
                vm.ls.get('loading').off();
            }).catch(function (error){
                console.log(error)
                vm.fm.error(error.data.errors[0]);
                
                $state.go('employee');
                vm.ls.get('loading').off();
            });;
        };
    
        this._createPostData = function(data){
            var remainingDaysOff = 0;
            for(let i=0;i < vm.employee_types.length; i++){
                if(vm.employee_types[i]._id == data.employee_type_id) {
                    remainingDaysOff = vm.employee_types[i].remaining_days_off;
                }
            }
            
            var postData = {
                _id: data._id,
                fullname: data.fullname,
                dob: data.dob,
                gender: data.gender,
                status_id: data.status_id,
                father_name: data.father_name,
                nationality_id: data.nationality_id,
                pasport_number: data.pasport_number,
                address: data.address,
                city: data.city,
                country_id: data.country_id,
                hp: data.hp,
                remaining_days_off: remainingDaysOff,
                phone_number: data.phone_number,
                parent_id: data.parent_id,
                email: data.email,
                employee_code: data.employee_code,
                employee_status_id: data.employee_status_id,
                employee_type_id: data.employee_type_id,
                departement_id: data.departement_id,
                join_date: data.join_date,
                photo: data.photo.filename ? data.photo.filename : data.photo,
                username: data.username,
                password: data.password
            };
            
            return postData;
        };

        this.generate = function(param){
            vm.ls.get('loading').on();
            ApiService.GenerateAuto.gen(param).then(function(resp) {
                var data = resp.data;
                if(Object.keys(param).length == 3){
                    vm.data.username = data.data.gen;
                } else {
                    vm.data.password = data.data.gen;
                }
                
                vm.ls.get('loading').off();
            });
        }
    
        this.delete = function(id) {
            vm.ls.get('loading').on();
            ApiService.Employee.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm._doSearch();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading').off();
            });
        };

        // Redirect to correct route
        this.doSearch = function() {
            this.search.order_by = this.orderBy.toString();
            $state.go('employee', this.search);
        };
        
        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    };
})();