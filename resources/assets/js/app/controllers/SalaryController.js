(function() {
    'use strict';
    
    angular.module('app').controller('SalaryController', SalaryController);
    
    function SalaryController($state, $window, $stateParams, ApiService, UploadFileService, LoadingService,
    FlashMessageService) {
        var vm = this;
        
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
    
        // Paginator
        this.paginator = {};
        
        this.uf = UploadFileService;
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this.selectedHash = null;
        this.propertyName = 'label';
        this.reverse = true;
        this.prop = 0;
        this.isStatus = true;
        this.isSelected = false;

        // Temp job in box bottom
        this.tempSalaryDescription = {
            _id: 0,
            salary_description_id: '',
            name: '',
            amount: 0,
            allowances_temp: 0,
            deductions_temp: 0
        };

        // Temp Id Employee
        this.tempEmployeeTags = [];

        this.createPriceDetail = function() {
            this.priceDetail = {
                total: (parseFloat(vm.data.basic_salary) + parseFloat(vm.data.positional_allowance) + parseFloat(vm.data.fixed_allowance)),
                allowances_temp: 0,
                deductions_temp: 0,
                grandTotal: 0,
            };
        }
    
        this.createEmptyData = function() {
            this.data = {
                _id: null,
                employee_id: null,
                basic_salary: 0,
                positional_allowance: 0,
                fixed_allowance: 0,
                detail: [],
                _delete_detail: []
            };
            return this.data;
        };

        this.setTitle = function() {
            this.title = "List Gaji";
        };
    
        this.init = function() {
            this.createEmptyData();
            this.setTitle();
            
            this.selectedHash = null;
            var search = {};

            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.is_actived)
                search.is_actived = $stateParams.is_actived;
            if($stateParams.approved)
                search.approved = $stateParams.approved;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
            
            // Order By
            if($stateParams.order_by) {
                
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }
    
            vm.search = search;
            vm.prop = 1;
            vm._doSearch();
        };

        this.loadEmployeeTag = function(param) {
            return ApiService.Employee.all({page: 'all', keyword: param}).then(
                function(resp) {
                    var data = resp.data;
                    var i = 0;
                    data.data.forEach(x => {
                        data.data[i].text = x.fullname;
                        i++;
                    })

                    vm.ls.get('loading').off();
                    return data.data;
            })
            .catch(function(error) {
                console.error ( error )
                return error.response;
            })
        }

        // Search invoice
        this._doSearch = function() {
            vm.ls.get('loading').on();
            vm.isSelected = false;
            ApiService.Salary.all(vm.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;

                vm.isStatus = vm.isSelectedAll();
                vm.list._delete_detail = [];
                vm.list.detail = [];

                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }
    
                // Paginator
                vm.paginator = Paginator(data);
    
                vm.ls.get('loading').off();
            });
        };

        // Add blank category
        this.add = function() {
            vm.createEmptyData();
            vm.prop = 2;
        };
    
        this.create = function() {
            vm.ls.get('loading').on();
            vm.ls.get('loadingsalary-descriptions').on();
            
            this.createEmptyData();
            
            var id = $stateParams.id;
            
            if(id){
                ApiService.Salary.get(id).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;
                        
                        vm.data = data;
                        vm.data.total = (data.basic_salary + data.positional_allowance + data.fixed_allowance)
                        
                        // Init for delete detail
                        data._delete_detail = [];

                        for(var i=0; i<vm.data.detail.length; i++) {
                            vm.data.detail[i]._hash = vm._getRandomHash();
                            vm.data.detail[i].salary_description_id = String(vm.data.detail[i].salary_description_id);
                            vm.data.detail[i].code = vm.data.detail[i].is_allowance === true ? "+" : "-";
                        }
                        
                        vm.groups = vm._groupingDetail(vm.data.detail);
                        vm.countAll();
                        
                    } else {
                        vm.error.on();
                        vm.message = resp.data.errors;
                    }
                    
                });
            }

            this.salaryDescription();
            this.loadEmployee();
        };
    
        // Submit to DB One
        this.submit = function(){
            vm.ls.get('loading').on();
            
            var postData = this._createPostData(this.data);
            ApiService.Salary.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    vm.init();
    
                    vm.fm.success(resp.data.message);
                    $state.go('salary');
                } else {
                    vm.fm.error(resp.data.message);
                }
    
                vm.ls.get('loading').off();
            });
        };

        // Submit Multiple to DB 
        this.submitMultiple = function(){
            vm.ls.get('loading').on();
            
            var postData = this._createPostData(this.data);
            ApiService.Salary.createMultipleEmployee(postData).then(function(resp){
                if(!resp.data.is_error) {
                    vm.init();
    
                    vm.fm.success(resp.data.message);
                    $state.go('salary');
                } else {
                    vm.fm.error(resp.data.message);
                }
    
                vm.ls.get('loading').off();
            });
        };

        this.loadEmployee = function() {
            ApiService.Employee.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.employees = data.data;
                
                vm.employees._id = String(vm.employees._id);
                
                vm.ls.get('loading').off();
            });
        }

        this.loadSalaryDescriptionDetail = function(id) {
            vm.ls.get('loadingsalary-descriptions').on();
            ApiService.SalaryDescription.get(id).then(function(resp) {
                var data = resp.data;
                
                vm.tempSalaryDescription = data.data;
                vm.tempSalaryDescription.salary_description_id = String(data.data._id);
                vm.tempSalaryDescription._id = null;
                vm.tempSalaryDescription.amount = 0;
                
                vm.tempSalaryDescription._hash = vm._getRandomHash();
                vm.ls.get('loadingsalary-descriptions').off();
            });
        };

        this.newDeskripsi = function() {
            this.tempSalaryDescription = {
                salary_description_id: '',
                name: '',
                amount: 0,
                allowances_temp: 0,
                deductions_temp: 0,
                _hash: this._getRandomHash()
            };
        };
    
        this._createPostData = function(data){
            var postData = JSON.parse(JSON.stringify(data));
            
            return postData;
        };

        this.delete = function(id) {
            vm.ls.get('loading').on();
            ApiService.Salary.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm._doSearch();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
                vm.ls.get('loading').off();
            });
        };

        this.salaryDescription = function() {
            ApiService.SalaryDescription.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.salary_descriptions = data.data;

                vm.salary_descriptions._id = String(vm.salary_descriptions._id);
                vm.ls.get('loadingsalary-descriptions').off();
            });
        }

        // Grouping Deskripsi details
        this._groupingDetail = function(details) {
            var gd = new GroupingDetail(details);

            return gd.get();
        };

        this.addDescription = function() {
            var isNew = true;
            if(this.tempSalaryDescription.code === "-"){
                this.tempSalaryDescription.amount = "-"+this.tempSalaryDescription.amount
            }

            // Check if this job update
            for(var i=0; i<this.data.detail.length; i++) {
                if(this.data.detail[i]._hash === this.tempSalaryDescription._hash) {
                    this.data.detail[i] = this.tempSalaryDescription;
                    isNew = false;
                }
            }
            
            if(isNew){
                // If new just push to detail
                var isError = false;
                for (var i = 0; i < this.data.detail.length; i++) {
                    if (this.tempSalaryDescription.salary_description_id == this.data.detail[i].salary_description_id) {
                        isError = true;
                        vm.fm.error('Deskripsi sudah ada.');
                    }
                }

                if (!isError) {
                    this.data.detail.push(this.tempSalaryDescription);
                }
            }
            
            this.groups = this._groupingDetail(this.data.detail);
            this.newDeskripsi();
            this.countAll();
            return;
        };

        this.countAll = function() {
            vm.createPriceDetail();
            
            for (var i = 0; i < vm.data.detail.length; i++) {
                if(vm.data.detail[i].code === "+") {
                    vm.priceDetail.allowances_temp += vm.data.detail[i].amount;
                } else {
                    vm.priceDetail.deductions_temp += vm.data.detail[i].amount;
                }
            }
            
            vm.priceDetail.grandTotal = (vm.priceDetail.total + vm.priceDetail.allowances_temp + vm.priceDetail.deductions_temp);
        };

        this.deleteDeskripsi = function(deskripsi) {
            if(deskripsi._id)
                this.data._delete_detail.push(deskripsi._id);
            
            this.data.detail.removeByObject(deskripsi);
            this.groups = this._groupingDetail(this.data.detail);
            this.countAll();
        }

        this.editSalaryDescription = function(salaryDescription) {
            salaryDescription.amount = Math.abs(salaryDescription.amount);
            this.tempSalaryDescription = salaryDescription;
        };

        // Add blank role
        this.update = function(x) {
            this.temp = {
                _id: x.id,
                is_checked: x.is_checked,
                _hash: Math.getRandomHash()
            };
            if(x.is_checked) {
                this.isSelected = true;
            } else {
                this.isSelected = false;
            }
            

            this.list.detail.push(this.temp);
        };
    
        this._getRandomHash = function() {
            var s1, s2;
            s1 = String(Math.floor(Math.random()*1000));
            s2 = String(new Date().getTime());
            return s1 + s2;
        };

        // Redirect to correct route
        this.doSearch = function() {
            this.search.order_by = this.orderBy.toString();
            $state.go('salary', this.search);
        };
        
        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };

        // if detail is selected all
        this.isSelectedAll = function() {
            var isSelectedAll = true;
            this.isSelected = true;
            
            return isSelectedAll;
        };

        // Select all checkbox
        this.selectAll = function() {
            vm.isStatus = false;
            vm.isSelected = true;
            
            for(var x = 0;x < vm.list.length;x++) {
                if (vm.list[x].is_checked == undefined || vm.list[x].is_checked == false) {
                    vm.list[x].is_checked = true;
                    vm.temp = {
                        _id: vm.list[x]._id,
                        is_checked: vm.list[x].is_checked,
                        _hash: Math.getRandomHash()
                    };
                    
                    vm.list.detail.push(vm.list[x].employee_id);
                } 
            }
        }

        // Select all checkbox
        this.unselectAll = function() {
            vm.isStatus = true;
            vm.isSelected = false;
            for(var x = 0;x < vm.list.length;x++)
            {
                if (vm.list[x].is_checked == true) {
                    vm.list[x].is_checked = false;
                    vm.temp = {
                        _id: vm.list[x]._id,
                        is_checked: false,
                        _hash: Math.getRandomHash()
                    };
                    vm.list.detail.removeByObject(vm.temp);
                    vm.list.detail.push(vm.list[x].employee_id);
                    vm.list.action = 'Action(Select All)'
                }
                
            }
        }

        this.loadEmployeeMultiple = function(param) {
            return ApiService.Employee.all({page: 'all', keyword: param}).then(
                function(resp) {
                    var data = resp.data;
                    var i = 0;
                    data.data.forEach(x => {
                        data.data[i].text = x.fullname;
                        i++;
                    })

                    vm.ls.get('loading').off();
                    return data.data;
            })
            .catch(function(error) {
                console.error ( error )
                return error.response;
            })
        }

        this.addAllowanceDeduction = function() {
            var isNew = true;
            if(this.tempSalaryDescription.code === "-"){
                this.tempSalaryDescription.amount = "-"+this.tempSalaryDescription.amount
            }
            // console.log('add Deskripsi', this.tempSalaryDescription);
            console.log('add Deskripsi', this.data);

            // Check if this job update
            for(var i=0; i<this.data.detail.length; i++) {
                if(this.data.detail[i]._hash === this.tempSalaryDescription._hash) {
                    this.data.detail[i] = this.tempSalaryDescription;
                    isNew = false;
                }
            }

            if(isNew){
                // If new just push to detail
                var isError = false;
                for (var i = 0; i < this.data.detail.length; i++) {
                    if (this.tempSalaryDescription.salary_description_id == this.data.detail[i].salary_description_id) {
                        isError = true;
                        vm.fm.error('Deskripsi sudah ada.');
                    }
                }

                if (!isError) {
                    this.data.detail.push(this.tempSalaryDescription);
                }
            }
            
            this.groups = this._groupingDetail(this.data.detail);
            this.newDeskripsi();
            this.countAll();
            return;
        };

        this.createAllowanceDeduction = function() {
            this.salaryDescription();
            this.data = {};
            this.data['detail'] = [];
            this.data['employee'] = [];
            var arrayId = $stateParams.arrayParam.split(',');
            
            arrayId.forEach(x => {
                ApiService.Employee.get(x).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;
                        
                        if(data) {
                            data['text'] = data['fullname'];
                        
                            vm.data['employee'].push(data._id);
                            vm.tempEmployeeTags.push(data);
                        }
                        
                    } else {
                        vm.error.on();
                        vm.message = resp.data.errors;
                    }
                })
            })
        }

        this.download = function(id){
            vm.ls.get('print').on();
            var params = {
                id: id
            };
            
            ApiService.Salary.download(params).then(function(resp) {
                if(!resp.data.is_error) {
                    $window.open(resp.data.data.url);
                    vm.ls.get('print').off();
                } else {
                    vm.fm.error(resp.data.message);
                    vm.ls.get('print').off();
                }
            });
        };
    };
})();