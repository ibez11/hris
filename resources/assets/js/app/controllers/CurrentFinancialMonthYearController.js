(function() {
    'use strict';
    
    angular.module('app').controller('CurrentFinancialMonthYearController', CurrentFinancialMonthYearController);
    
    function CurrentFinancialMonthYearController($state, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
    
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();

        // Paginator
        this.paginator = {};
        this.monthName = [ "January", "February", "March", "April", "May", "June", 
            "July", "August", "September", "October", "November", "December" ];
        
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this.selectedHash = null;
        this.propertyName = 'label';
        this.reverse = true;
        this.prop = 0;

        this.title = null;
    
        this.createEmptyData = function() {
            return {
                id: null,
                month: null,
                year: null,
                is_selected: false,
                _hash: Math.getRandomHash(),
            };
        };

        this.setTitle = function() {
            this.title = "List Bulan/Tahun Keuangan";
        };
    
        this.init = function() {
            this.setTitle();
            this.createEmptyData();
            this.selectedHash = null;
            var search = {};

            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.is_actived)
                search.is_actived = $stateParams.is_actived;
            if($stateParams.approved)
                search.approved = $stateParams.approved;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
    
            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'desc';
            }
    
            vm.search = search;
            vm.prop = 1;
            
            vm._doSearch();
            
        };
    
        // Search invoice
        this._doSearch = function() {
            vm.ls.get('loading').on();
            
            ApiService.CurrentFinancialMonthYear.all(vm.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;
                
                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }

                // Paginator
                vm.paginator = Paginator(data);

                var month = [];
            
                for(let i = 0; i <= 11; i++){
                    month.push({
                        id: parseInt(i),
                        name: vm.monthName[i]
                    });
                }
                vm.months = month;
                vm.months.id = String(vm.months.id);

                var year = new Date().getFullYear();
                var years = [];
                
                years.push({id: year});
                for (var i = 1; i < 5; i++) {
                    years.push({id: (year + i)});
                }
                vm.years = years;
                vm.years.id = String(vm.years.id);

                vm.ls.get('loading').off();
            });
        };

        // Add blank category
        this.add = function() {
            vm.createEmptyData();
            vm.prop = 2;
        };
    
        this.create = function() {
            vm.ls.get('loading').on();
            this.createEmptyData();
            
            var id = $stateParams.id;
            if(id){
                ApiService.CurrentFinancialMonthYear.get(id).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;

                        vm.data = data;
                    } else {
                        vm.error.on();
                        vm.message = resp.data.errors;
                        $state.go('currentfinancialmonthyear', this.search);
                    }
                });
            }
        };
    
        // Submit to DB
        this.submit = function(currentfinancialmonthyear){
            vm.ls.get('loading').on();
            var postData = this._createPostData(currentfinancialmonthyear);
            
            ApiService.CurrentFinancialMonthYear.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    vm.init();
    
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
    
                vm.ls.get('loading').off();
            });
    
        };
    
        this._createPostData = function(data){
            var postData = {
                _id: data._id,
                month: data.month,
                year: data.year,
                is_selected: data.is_selected
            };
    
            return postData;
        };
    
        this.delete = function(id) {
            vm.ls.get('loading').on();
            ApiService.CurrentFinancialMonthYear.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm._doSearch();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading').off();
            });
        };

        this.showPage = function(page) {
            this.search.page = page;
            this.doSearch();
        };

        // Redirect to correct route
        this.doSearch = function() {
            this.search.order_by = this.orderBy.toString();
            $state.go('currentfinancialmonthyear', this.search);
        };

        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };

        this.editIsSelected = function(param) {
            var param = {
                _id: param._id,
                is_selected: param.is_selected
            }
            
            ApiService.CurrentFinancialMonthYear.setSelected(param).then(function(resp){
                if(!resp.data.is_error) {
                    vm.init();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
    
                vm.ls.get('loading').off();
            });
        }

        this.monthEnd = function() {
            ApiService.CurrentFinancialMonthYear.monthEnd().then(function(resp){
                if(!resp.data.is_error) {
                    vm.init();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
    
                vm.ls.get('loading').off();
            });
        }
    };
})();