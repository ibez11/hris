(function() {
    'use strict';
    
    angular.module('app').controller('PaidLeaveController', PaidLeaveController);
    
    function PaidLeaveController($state, $window, $stateParams, ApiService, UploadFileService, LoadingService,
    FlashMessageService) {
        var vm = this;
        
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
        this.listPending = false;
    
        // Paginator
        this.paginator = {};
        
        this.uf = UploadFileService;
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this.selectedHash = null;
        this.propertyName = 'label';
        this.reverse = true;
        this.prop = 0;

        this.createEmptyData = function() {
            this.data = {
                _id: null,
                start_date: null,
                end_date: null,
                category_leave_id: null,
                status: {
                    name: 'draft'
                },
                notes: '',
            };
            return this.data;
        };

        this.setTitle = function() {
            this.title = "List Cuti";
        };

        this.isCreate = function() {
            return !$stateParams.id;
        };
    
        this.isDraft = function() {
            return this.isStatus('draft') || $stateParams.parent_id;
        };
    
        this.isPending = function() {
            return this.isStatus('pending');
        };

        this.isStatus = function(status) {
    
            return this.data.status.name === status;
        };
    
        this.init = function() {
            this.createEmptyData();
            this.setTitle();
            
            this.selectedHash = null;
            var search = {};

            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.status)
                search.status = $stateParams.status;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
            if($stateParams.status_page == 'pending')
                this.listPending = true;
            
            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }
    
            vm.search = search;
            vm.prop = 1;
            vm._doSearch();
        };

        // Search Paid Leave
        this._doSearch = function() {
            if (this.listPending == true) {
                vm.search.status = 'pending';
            }
            vm.ls.get('loading').on();
            ApiService.PaidLeave.all(vm.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;
                
                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }
    
                // Paginator
                vm.paginator = Paginator(data);
    
                vm.ls.get('loading').off();
            });
        };

        // Add blank category
        this.add = function() {
            vm.createEmptyData();
            vm.prop = 2;
        };
    
        this.create = function() {
            vm.ls.get('loading').on();
            this.createEmptyData();
            
            var id = $stateParams.id;
            
            if(id) {
                ApiService.PaidLeave.get(id).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;
                        
                        vm.data = data;
                    } else {
                        vm.error.on();
                        vm.message = resp.data.errors;
                    }
                });
            }

            ApiService.CategoryPaidLeave.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.category_paid_leave = data.data;
                
                vm.category_paid_leave._id = String(vm.category_paid_leave._id);
                vm.ls.get('loading').off();
            });
        };

        // Submit to DB
        this.submit = function(){
            vm.ls.get('loading').on();
            
            var postData = this._createPostData(this.data);

            if(vm.sendToManager == 1) {
                postData.status = 'pending';
            }
            vm.sendToManager = 0; // Set back to 0

            ApiService.PaidLeave.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    vm.init();
    
                    vm.fm.success(resp.data.message);
                    $state.go('paid-leave');
                } else {
                    vm.fm.error(resp.data.message);
                }
    
                vm.ls.get('loading').off();
            });
        };

        this.download = function(id){
            vm.ls.get('print').on();
            var params = {
                id: id
            };
            
            ApiService.PaidLeave.download(params).then(function(resp) {
                if(!resp.data.is_error) {
                    $window.open(resp.data.data.url);
                    vm.ls.get('print').off();
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
            });
        };

        this._createPostData = function(data){
            var postData = JSON.parse(JSON.stringify(data));
            
            return postData;
        };

        this.delete = function(id) {
            vm.ls.get('loading').on();
            ApiService.PaidLeave.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm._doSearch();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
                vm.ls.get('loading').off();
            });
        };

        this._getRandomHash = function() {
            var s1, s2;
            s1 = String(Math.floor(Math.random()*1000));
            s2 = String(new Date().getTime());
            return s1 + s2;
        };

        // Redirect to correct route
        this.doSearch = function() {
            if(this.listPending == true){
                this.search.order_by = this.orderBy.toString();
                this.search.status_page = 'pending';
                $state.go('paid-leave-pending', this.search);
            } else {
                this.search.order_by = this.orderBy.toString();
                $state.go('paid-leave', this.search);
            }
        };
        
        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    };
})();