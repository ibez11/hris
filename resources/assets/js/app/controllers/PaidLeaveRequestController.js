(function() {
    'use strict';
    
    angular.module('app').controller('PaidLeaveRequestController', PaidLeaveRequestController);
    
    function PaidLeaveRequestController($state, $window, $stateParams, ApiService, UploadFileService, LoadingService,
    FlashMessageService) {
        var vm = this;
        
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
        this.listPending = false;
    
        // Paginator
        this.paginator = {};
        
        this.uf = UploadFileService;
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this.selectedHash = null;
        this.propertyName = 'label';
        this.reverse = true;
        this.prop = 0;

        this.createEmptyData = function() {
            this.data = {
                _id: null,
                start_date: null,
                end_date: null,
                category_leave_id: null,
                status: {
                    name: 'draft'
                },
                notes: '',
            };
            return this.data;
        };

        this.setTitle = function() {
            this.title = "List Permintaan Cuti";
        };
    
        this.init = function() {
            this.createEmptyData();
            this.setTitle();
            
            this.selectedHash = null;
            var search = {};

            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.status)
                search.status = $stateParams.status;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
            if($stateParams.status_page == 'pending')
                this.listPending = true;
            
            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }
    
            vm.search = search;
            vm.prop = 1;
            vm._doSearch();
        };

        // Search Paid Leave
        this._doSearch = function() {
            if (this.listPending == true) {
                vm.search.status = 'pending';
            }
            vm.ls.get('loading').on();
            ApiService.PaidLeaveRequest.all(vm.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;
                
                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }
    
                // Paginator
                vm.paginator = Paginator(data);
    
                vm.ls.get('loading').off();
            });
        };

        this.create = function() {
            vm.ls.get('loading').on();
            this.createEmptyData();
            
            var id = $stateParams.id;
            
            if(id) {
                ApiService.PaidLeaveRequest.get(id).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;
                        
                        vm.data = data;
                    } else {
                        vm.error.on();
                        vm.message = resp.data.errors;
                    }
                });
            }

            ApiService.CategoryPaidLeave.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.category_paid_leave = data.data;
                
                vm.category_paid_leave._id = String(vm.category_paid_leave._id);
                vm.ls.get('loading').off();
            });
        };

        this.setApproved = function(id) {
            this.setStatus(id, 'accepted');
            this.create();
        };

        this.setRevision = function(id) {
            this.setStatus(id, 'rejected');
            this.create();
        };

        this.setStatus = function(id, status) {
            vm.ls.get('loading-' + id).on();
            ApiService.PaidLeaveRequest.setStatus(id, status).then(function(resp) {
                if(!resp.data.is_error) {
                    //For refresh / re-init
                    if (vm.listPending == true) {
                        vm.init();
                    } else {
                        vm.create();
                    }
                    vm.fm.success(resp.data.message);
                    $state.go('paid-leave-request', this.search);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading-' + id).off();
            });
        };

        this.download = function(id){
            vm.ls.get('print').on();
            var params = {
                id: id
            };
            
            ApiService.PaidLeave.download(params).then(function(resp) {
                if(!resp.data.is_error) {
                    $window.open(resp.data.data.url);
                    vm.ls.get('print').off();
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
            });
        };

        this._getRandomHash = function() {
            var s1, s2;
            s1 = String(Math.floor(Math.random()*1000));
            s2 = String(new Date().getTime());
            return s1 + s2;
        };

        // Redirect to correct route
        this.doSearch = function() {
            if(this.listPending == true){
                this.search.order_by = this.orderBy.toString();
                this.search.status_page = 'pending';
                $state.go('paid-leave-request-pending', this.search);
            } else {
                this.search.order_by = this.orderBy.toString();
                $state.go('paid-leave-request', this.search);
            }
        };
        
        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    };
})();