(function() {

'use strict';

angular.module('app').controller('MainController', MainController);
function MainController($http, $interval, ApiService, AuthService, FlashMessageService,
AccessControlService, SelectedMonthYearTransactionService, $scope, $state) {
  $scope.$state = $state;
  
  var vm = this;

  this.loaded = false;
  this.viewLoadedList = [];
  this.messages = [];
  this.ac = AccessControlService;
  this.smyt = SelectedMonthYearTransactionService;

  this.changeSelected;
  this.request_leave_count = 0;
  this.selected_transactions = {};
  this.month_year_financials = {};
  this.user = AuthService;
  this.fm = FlashMessageService;

  this.init = function() {
    // Run interval
    this.fm.start();

    // ApiService.Account.Profile.get().then(function(resp) {
    //     var data = resp.data;
    //     vm.user = data.data;
        
    //     AccessControlService.setUser(vm.user);
    // });
    $scope.$watch('main.selected_transactions', function(value) {
      if(typeof value == 'object') {
        vm.changeSelected(vm.selected_transactions);
      }
    });

    this.changeSelected = function(value) {
      if(typeof value == 'string')
      ApiService.CurrentFinancialMonthYear.get(value).then(function(resp) {
        var data = resp.data;
        var x = data.data;
        
        var setCookie =  DateFormatter.dateCustomize(new Date(), 'YYYY/MM');
        if(Object.keys(x).length) {
          x.month = parseFloat(x.month) + 1;

          if(x.month < 9)
          x.month = '0'+x.month;

          var month = x.month;
          var year = x.year;
          vm.selected_transactions = x;
          
          setCookie = year+"/"+month;
        } 
        
        vm.smyt.set(setCookie);
      })
    }
    
    if(!this.smyt.get()) {
      var data ={
        _id: null,
        month: DateFormatter.dateCustomize(new Date(), 'MM'),
        year: DateFormatter.dateCustomize(new Date(), 'YYYY'),
      }
      this.smyt.set(data);
    }
    

    this.selected_transactions = this.smyt.get()

    ApiService.CurrentFinancialMonthYear.all({}).then(function(resp) {
      var data = resp.data;
      data.data.forEach(x => {
        x.month = parseFloat(x.month) + 1;
        if(x.is_selected)
        vm.selected_transactions = x;
        
        if(x.month < 9)
        x.month = '0'+x.month;
      })
      vm.month_year_financials = data.data;

      vm.month_year_financials._id = String(vm.month_year_financials._id);
    });

    ApiService.PaidLeaveRequest.all({page: 'all'}).then(function(resp) {
      var data = resp.data;
      vm.request_leave_count = data.data.length > 0;
    });

    setInterval(function() {
      $('body').layout('fix');
      $('body').layout('fixSidebar');
      // $.AdminLTE.layout.activate();
    }, 3000);

    var _closure = null;
    for(var i=0; i<viewList.length; i++) {
        this.viewLoadedList[i] = false;
        _closure = function(index) {
            $http.get(viewList[index]).then(function(data) {
                vm.viewLoadedList[index] = true;
                // console.log('get ' + index);
            });
        };

        _closure(i);
    };

    var id = $interval(function(){
        if(vm.isLoaded()) {
            vm.loaded = true;
            
            $interval.cancel(id);
        }
    }, 500);
  };

  this.removeMessage = function(x) {
      this.messages.removeByObject(x);
  }

  this.isLoaded = function() {
    var count = 0;
    var x = 0;

    for(var i=0; i<this.viewLoadedList.length; i++) {
        x = this.viewLoadedList[i];
        if(x == true)
            count++;
    };

    return count >= viewList.length;
  }

  this.logout = function() {
      ApiService.Auth.logout().then(function(resp) {
        window.location = '/logout';
      });
  };
};

})();
