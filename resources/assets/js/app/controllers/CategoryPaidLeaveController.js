(function() {
    'use strict';
    
    angular.module('app').controller('CategoryPaidLeaveController', CategoryPaidLeaveController);
    
    function CategoryPaidLeaveController($state, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
    
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
    
        // Paginator
        this.paginator = {};
        
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this.selectedHash = null;
        this.propertyName = 'label';
        this.reverse = true;
        this.prop = 0;
    
        this.createEmptyData = function() {
            return {
                id: null,
                name: null,
                label: null,
                _hash: Math.getRandomHash(),
            };
        };
    
        this.init = function() {
            this.createEmptyData();
            this.selectedHash = null;
            var search = {};

            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.is_actived)
                search.is_actived = $stateParams.is_actived;
            if($stateParams.approved)
                search.approved = $stateParams.approved;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
    
            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }
    
            vm.search = search;
            vm.prop = 1;
            vm._doSearch();
        };
    
        // Search invoice
        this._doSearch = function() {
            vm.ls.get('loading').on();
            
            ApiService.CategoryPaidLeave.all(this.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;
                vm.list.forEach(function(x) {
                    x._hash = Math.getRandomHash();
                    x.prop = 1;
                })
    
                vm.ls.get('loading').off();
            });
        };

        // Add blank category
        this.add = function() {
            vm.createEmptyData();
            vm.prop = 2;
        };
    
        this.create = function() {
            vm.ls.get('loading').on();
            this.createEmptyData();
            var id = $stateParams.id;
            if(id){
                ApiService.CategoryPaidLeave.get(id).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;

                        vm.data = data;
                    } else {
                        vm.error.on();
                        vm.message = resp.data.errors;
                        $state.go('category-paid-leave', this.search);
                    }
                });
            }
        };
    
        // Submit to DB
        this.submit = function(categorypaidleave){
            vm.ls.get('loading').on();
            var postData = this._createPostData(categorypaidleave);
            
            ApiService.CategoryPaidLeave.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    vm.init();
    
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
    
                vm.ls.get('loading').off();
            });
    
        };
    
        this._createPostData = function(data){
            var postData = {
                _id: data._id,
                name: data.name,
                label: data.label
            };
    
            return postData;
        };
    
        this.delete = function(id) {
            vm.ls.get('loading').on();
            ApiService.CategoryPaidLeave.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm._doSearch();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading').off();
            });
        };

        // Redirect to correct route
        this.doSearch = function() {
            this.search.order_by = this.orderBy.toString();
            $state.go('category-paid-leave', this.search);
        };

        // Edit CategoryPaidLeave
        this.edit = function(categorypaidleave) {
            vm.selectedHash = categorypaidleave._hash;
        };
  
        
        this.sortBy = function(columnName) {
            vm.reverse = (vm.propertyName === columnName) ? !vm.reverse : false;
            vm.propertyName = columnName;
            
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    };
})();