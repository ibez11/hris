(function() {

'use strict';

angular.module('app').config(config);

function config($stateProvider, $urlRouterProvider, $httpProvider) {
    console.log('path', path);

    var states = [];
    var state = null;

    state = {
        name: 'dashboard',
        url: '/dashboard',
        templateUrl: path.dashboard,
        controller: 'DashboardController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'departement',
        url: '/departement/list?keyword&order_by&page',
        templateUrl: path.departement.index,
        controller: 'DepartementController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'category-paid-leave',
        url: '/category-paid-leave/list?keyword&order_by&page',
        templateUrl: path.category_paid_leave.index,
        controller: 'CategoryPaidLeaveController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'currentfinancialmonthyear',
        url: '/currentfinancialmonthyear/list?keyword&order_by&page',
        templateUrl: path.currentfinancialmonthyear.index,
        controller: 'CurrentFinancialMonthYearController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'employee',
        url: '/employee/list?keyword&order_by&page',
        templateUrl: path.employee.index,
        controller: 'EmployeeController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'employee-create',
        url: '/employee/create',
        templateUrl: path.employee.create,
        controller: 'EmployeeController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'employee-detail',
        url: '/employee/detail/{id}',
        templateUrl: path.employee.create,
        controller: 'EmployeeController',
        controllerAs: 'vm'
    };

    states.push(state);

    state = {
        name: 'salary',
        url: '/salary/list?keyword&order_by&page',
        templateUrl: path.salary.index,
        controller: 'SalaryController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'salary-create',
        url: '/salary/create',
        templateUrl: path.salary.create,
        controller: 'SalaryController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'salary-allowance-deduction-create',
        url: '/salary/allowance-deduction/create/{arrayParam:string}',
        templateUrl: path.salary.allowance_dedcution,
        controller: 'SalaryController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'salary-detail',
        url: '/salary/detail/{id}',
        templateUrl: path.salary.create,
        controller: 'SalaryController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'announcement',
        url: '/announcement/list?keyword&order_by&page',
        templateUrl: path.announcement.index,
        controller: 'AnnouncementController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'announcement-create',
        url: '/announcement/create',
        templateUrl: path.announcement.create,
        controller: 'AnnouncementController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'announcement-detail',
        url: '/announcement/detail/{id}',
        templateUrl: path.announcement.create,
        controller: 'AnnouncementController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'paid-leave',
        url: '/paid-leave/list?keyword&status&order_by&page',
        templateUrl: path.paidleave.index,
        controller: 'PaidLeaveController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'paid-leave-pending',
        url: '/paid-leave/list/{status_page}?order_by&page',
        templateUrl: path.paidleave.index,
        controller: 'PaidLeaveController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'paid-leave-create',
        url: '/paid-leave/create',
        templateUrl: path.paidleave.create,
        controller: 'PaidLeaveController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'paid-leave-detail',
        url: '/paid-leave/detail/{id}',
        templateUrl: path.paidleave.create,
        controller: 'PaidLeaveController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'paid-leave-request-approve',
        url: '/paid-leave-request/approve/{id}',
        templateUrl: path.paidleaverequest.approve,
        controller: 'PaidLeaveRequestController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'paid-leave-request',
        url: '/paid-leave-request/list?keyword&status&order_by&page',
        templateUrl: path.paidleaverequest.index,
        controller: 'PaidLeaveRequestController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'setting',
        url: '/setting',
        templateUrl: path.setting.index,
        controller: 'SettingController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    for(var i=0; i<states.length; i++)
        $stateProvider.state(states[i]);

    $urlRouterProvider.otherwise('/dashboard');
    // alternatively, register the interceptor via an anonymous factory
    $httpProvider.interceptors.push(function($q) {
      return {
        'response': function(resp) {
            var data = resp.data;

            if(data.status === 401) {
                // Force logout
                window.location = '/logout';
            }

            return resp;
        }
      };
    });
}

})();
